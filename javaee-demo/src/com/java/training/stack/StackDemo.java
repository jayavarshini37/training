/*
Requirement:
    To Create a stack using generic type and implement
    Push atleast 5 elements
    Pop the peek element
    Search a element in stack and print index value
    Print the size of stack
    Print the elements using Stream
    Reverse List Using Stack with minimum 7 elements in list.

Entity:
    StackDemo 

Method Signature:
    public static void main(String [] args)

Jobs to be done :  
    1.Create the Stack with generic String type and create stream for displaying the Stack elements.
        1.1 Using push() method push 5 values into the stack
        1.2 Using pop() method, display the peek element of the stack
    2.Using search() method search the specified value 
        2.1 Using size() method,find the size of the stack
        2.2Print the stack size
            2.2.1 Also print the index value of the stack
    3.Print the stack values using stream and for each.
    4.The element is removed from stack using remove method
    5.Element is pop from the stack and  it is added to the list
    6.Remove the elements from the stack
        6.1 Add it to the list using add() method.

Pseudo code
public class StackDemo {
    public static void main(String [] args) {
        Stack<String> fruits = new Stack<String>();  //Creating Stack using generic type
        Stream<String> stream = fruits .stream();
        fruits.push();
        fruits.push();
        fruits.push();
        fruits.push(); 
        fruits.add();        
        fruits.pop();   //pop top element in stack
        System.out.println("Create a stack using generic type");
        int index = fruits.search();  //Initialize search variable
        int size = fruits.size();        //Initialize size variable
        System.out.println(size);       //Printing the stack size 
        System.out.println(index);      //Printing the index of the element
        stream.forEach((element) -> {
           System.out.println(element);  // print element
        });    //Process Stack Using Stream
        
        System.out.println("Reverse the List Using Stack with minimum 7 elements in the list");
        Stack<String> stack = new Stack<String>();
        while(fruits.size() > 0) {
                  stack.push(fruits.remove(0)); //Element is removed from list and push to stack
        }

        while(stack.size() > 0){
        	fruits.add(stack.pop()); //Element is pop from stack and add to list
        } 

        System.out.println(fruits);
    }
}

*/
package com.java.training.stack;

import java.util.Stack;
import java.util.stream.Stream;
public class StackDemo {
    public static void main(String [] args) {
        Stack<String> fruits = new Stack<String>();  //Creating Stack using generic type
        Stream<String> stream = fruits .stream();
        fruits.push("Mango");
        fruits.push("Apple");
        fruits.push("Watermelon");
        fruits.push("Custard Apple"); 
        fruits.add("Pomegranate");        
        fruits.pop();   //pop top element in stack
        System.out.println("Create a stack using generic type");
        int index = fruits.search("Apple");  //Initialize search variable
        int size = fruits.size();        //Initialize size variable
        System.out.println(size);       //Printing the stack size 
        System.out.println(index);      //Printing the index of the element
        stream.forEach((element) -> {
           System.out.println(element);  // print element
        });    //Process Stack Using Stream
        
        System.out.println("Reverse the List Using Stack with minimum 7 elements in the list");
        Stack<String> stack = new Stack<String>();
        while(fruits.size() > 0) {
                  stack.push(fruits.remove(0)); //Element is removed from list and push to stack
        }

        while(stack.size() > 0){
        	fruits.add(stack.pop()); //Element is pop from stack and add to list
        } 

        System.out.println(fruits);
    }
}
