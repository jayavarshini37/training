/*
Requirement:
    To create a list and to add 10 elements 
    1. Create a package com.java.training.list
    2. Create another list and to use add all method
    3. Use indexOf() and lastIndexOf() methods
    4. To print the elements using for loops and iterator and stream
    5. To convert the list to string and array.

Entity:
    public class ListDemo
    
Method Signature:
    public static void main(String[] args)
    indexOf(),
    lastIndex(),
    list.Stream(), 
    hasNext(), 
    next()

Jobs to be done:
    1.Create a list and add the elements into the list
        1.i)Print the list
    2.Create another list named list1 and add the elements into the list
        2.i)Print the list
    3.Add all the elements of list1 to list using addAll() method.
        3.i)Print the modified list
        3.ii)Print the index of first element
        3.iii)Print the index of last element
        3.iv)Print the elements using enhanced for loop
    4.Create an array 
        4.i)Insert the elements in the list to array
        4.ii)Print the list in the array
    5.Create a set
        5.i) Add the list elements into the set
        5.ii)Using for loop assign the variable value1 and store 
            the elements in the variable value1
        5.iii) Print the set
    6.Print the elements using for loop
    7.Create an iterator
        7.i)Print the list
    8.Print the list using stream.

Pseudo code:
public class ListDemo {
    
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        // Add the elements to the list.
        list.add();
        list.add();
        list.add();
        list.add();
        list.add();
        list.add();
        list.add();
        list.add();
        list.add();
        list.add();
        // Print the list.
        System.out.println("The list is" + " " + list);
        
        List<String> list1 = new ArrayList<>();
        list1.add();
        list1.add();
        list1.add();
        System.out.println("The Second list is" + " " + list1);
        
        // Now add all elements in list1 to list
        list.addAll(list1);
        
        // print the list
        System.out.println("The Modified list is " + " " + list);
        
        // Print the index of first element
        System.out.println("The first index is" + " " + list.indexOf("Orange"));
        
        // print the index of last element
        System.out.println("The last index is" + " " + list.lastIndexOf("Purple"));
        
        // print the elements using enhanced for loop
        for (String value : list) {
            System.out.println("The elements are" + value);
        }
        
        // Creating an array for a list
        String[] colorArray = new String[list.size()];
        colorArray = list.toArray(colorArray);
        System.out.println("The list in array "+ " " + Arrays.toString(colorArray));
        
        // Converting List into an Set
        Set<String> alphaSet = new HashSet<>(list);
        System.out.println("The Set Values are" );
        for (String value1 : alphaSet) {
            System.out.println(value1);
        }
        
        // print the elements using for loop
        for (int integer = 0; integer < list1.size(); integer++) {
            System.out.println("The elements are" + " " + list1);
        }
        
        // Using Iterator
        Iterator<String> iteratorList = list.iterator();
        while (iteratorList.hasNext()) {
            System.out.println("The elements are" + " " + iteratorList.next());
        }
        
        // Using Stream
        list.stream().forEach ((element) -> System.out.println(element));
    }
        
}    
*/
package com.java.training.list;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

public class ListDemo {
    
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        // Add the elements to the list.
        list.add("Black");
        list.add("White");
        list.add("Orange");
        list.add("Pink");
        list.add("Gray");
        list.add("Indigo");
        list.add("Violet");
        list.add("Brown");
        list.add("Purple");
        list.add("Yellow");
        // Print the list.
        System.out.println("The list is" + " " + list);
        
        List<String> list1 = new ArrayList<>();
        list1.add("Red");
        list1.add("Green");
        list1.add("Blue");
        System.out.println("The Second list is" + " " + list1);
        
        // Now add all elements in list1 to list
        list.addAll(list1);
        
        // print the list
        System.out.println("The Modified list is " + " " + list);
        
        // Print the index of first element
        System.out.println("The first index is" + " " + list.indexOf("Orange"));
        
        // print the index of last element
        System.out.println("The last index is" + " " + list.lastIndexOf("Purple"));
        
        // print the elements using enhanced for loop
        for (String value : list) {
            System.out.println("The elements are" + value);
        }
        
        // Creating an array for a list
        String[] colorArray = new String[list.size()];
        colorArray = list.toArray(colorArray);
        System.out.println("The list in array "+ " " + Arrays.toString(colorArray));
        
        // Converting List into an Set
        Set<String> alphaSet = new HashSet<>(list);
        System.out.println("The Set Values are" );
        for (String value1 : alphaSet) {
            System.out.println(value1);
        }
        
        // print the elements using for loop
        for (int integer = 0; integer < list1.size(); integer++) {
            System.out.println("The elements are" + " " + list1);
        }
        
        // Using Iterator
        Iterator<String> iteratorList = list.iterator();
        while (iteratorList.hasNext()) {
            System.out.println("The elements are" + " " + iteratorList.next());
        }
        
        // Using Stream
        list.stream().forEach ((element) -> System.out.println(element));
    }
        
}