/*
Requirement:
    To Explain about contains, retainAll() and subString

Entity:
    public class Method

Method Signature:
    public static void main(String[] args)

Jobs to be done:
1. Create a list named integer and add the elements into the list
    2. Using contains keyword, check whether the element is present in the list
        2.1.if the element is present
            2.1.i) Print "It is true"
        2.2 If the element is not present
            2.2.i) Print "it is false
    3. Create a list named oddInteger and add the elements into the list
        3.1 Using retain method retain all the matching elements between integer
            and oddInteger
    4. Create a list named String and add the elements into this list
        4.1 Print all the elements
    5. Create another list named subString  
        5.1 Using subList method all the elements from the string list
        5.2 Print the elements 

Pseudo code
public class Method {
    
    public static void main(String[] args) {
        List<Integer> integer = new ArrayList<>();
        integer.add();
        integer.add();
        integer.add();
        
        // contains is used to check whether the value is present
        if (integer.contains()==true) {
            System.out.println("It is present");
        }else {
            System.out.println ("It is not present");
        }
        
        // retainAll() method used to retain all the matching elements
        List<Integer> oddInteger = new ArrayList<>();
        oddInteger.add();
        oddInteger.add();
        oddInteger.add();
        integer.retainAll(oddInteger);
        System.out.println(" The value is"+"" + integer);
        
        // Sublist is used to produce the desired values from the given indexes.
        List<String> string = new ArrayList<>();
        string.add();
        string.add();
        string.add();
        string.add();
        string.add();
        System.out.println(" The List is" + string);
        
        // now create another list
        List<String> subString = new ArrayList<>();
        subString = string.subList(0,3);
        System.out.println("The sublist is " + " " + subString);
    }

}
*/
package com.java.training.list;

import java.util.List;
import java.util.ArrayList;

public class Method {
    
    public static void main(String[] args) {
        List<Integer> integer = new ArrayList<>();
        integer.add(10);
        integer.add(20);
        integer.add(30);
        
        // contains is used to check whether the value is present
        if (integer.contains(20)==true) {
            System.out.println("It is present");
        }else {
            System.out.println ("It is not present");
        }
        
        // retainAll() method used to retain all the matching elements
        List<Integer> oddInteger = new ArrayList<>();
        oddInteger.add(10);
        oddInteger.add(20);
        oddInteger.add(25);
        integer.retainAll(oddInteger);
        System.out.println(" The value is"+"" + integer);
        
        // Sublist is used to produce the desired values from the given indexes.
        List<String> string = new ArrayList<>();
        string.add("Audi");
        string.add("Ferrari");
        string.add("BMW");
        string.add("Volvo");
        string.add("Jaguar");
        System.out.println(" The List is" + string);
        
        // now create another list
        List<String> subString = new ArrayList<>();
        subString = string.subList(0,3);
        System.out.println("The sublist is " + " " + subString);
    }

}