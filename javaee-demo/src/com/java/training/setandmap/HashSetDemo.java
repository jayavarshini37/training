/*
Requirement
    Java program to demonstrate adding elements, displaying, removing, and iterating in hash set
 
Entity:
    HashSet
   
Method Signature
    1.void add(Set<S> set, int val);
    2.void remove(Set<S> set, int val);
    3.void fullSet(Set<S> set);
  
Jobs to be Done
    1.Create a HashSet 
    2.Get the Value from the User.
    3.Call void add method to add elements to Set.
    4.Call void remove to remove elements to Set. 
    5.Call void fullSet method to display the Elements
    6.Use stream API to iterate the Set.
 
Pseudo Code
class HashSetDemo {
 	
 	public HashSetDemo() {
 		
 	}
 	
 	void remove(Set<Integer> set, Integer val) {
 		set.remove(val);
 	}

 	void fullSet(Set<Integer> set) {
 		set.stream().forEach(x -> System.out.println(x));
 	}
 	
 	public static void main(String[] args) {
 		HashSetDemo hash = new HashSetDemo();
 		
 		Scanner scanner = new Scanner(System.in);
 		
 		Set<Integer> newSet = new HashSet<Integer>();
 		
 		newSet.add(10);
 		newSet.add(20);
 		newSet.add(30);
 		newSet.add(40);
 		newSet.add(50);
 		newSet.add(60);
 		
 		hash.fullSet(newSet);
 		
 		System.out.println("Enter the Value to Remove");
 		
 		Integer val = scanner.nextInt();
 		
 		hash.remove(newSet, val);
 		
 		newSet.stream().forEach(x -> System.out.println(x));
 		
 		
	} 

}
 
 */
package com.java.training.setandmap;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class HashSetDemo {
	
	public HashSetDemo() {
		
	}
	
	void remove(Set<Integer> set, Integer val) {
		set.remove(val);
	}
	
	void fullSet(Set<Integer> set) {
		set.stream().forEach(x -> System.out.println(x));
	}
	
	public static void main(String[] args) {
		HashSetDemo hash = new HashSetDemo();
		
		try (Scanner scanner = new Scanner(System.in)) {
			Set<Integer> newSet = new HashSet<Integer>();
			
			newSet.add(10);
			newSet.add(20);
			newSet.add(30);
			newSet.add(40);
			newSet.add(50);
			newSet.add(60);
			
			hash.fullSet(newSet);
			
			System.out.println("Enter the Value to Remove");
			
			Integer val = scanner.nextInt();
			
			hash.remove(newSet, val);
			
			newSet.stream().forEach(x -> System.out.println(x));
		}
		
		
	}

}