/*
Requirement
    Java program to demonstrate insertions and string buffer in tree set
  
Entity
    1.TreeSetDemo
    
Method Signature
    public static void main(String[] args) 

Jobs to be Done
    1.Create a TreeSet
    2.Add values to TreeSet
    3.Print the Elements in TreeSet.
  
Pseudo Code
class TreeSetExercise {
  
    public static void main(String[] args) {
       
        Set<String> newSet = new TreeSet<String>();
       
       	newSet.add(new StringBuffer("Kaviya").toString());
       	newSet.add(new StringBuffer("Keerthana").toString());
       	newSet.add(new StringBuffer("Keerthi").toString());
       	newSet.add(new StringBuffer("Kowsi").toString());
       
       	newSet.stream().forEach(x -> System.out.println(x));
       }
  }

*/
package com.java.training.setandmap;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo {
	
	public static void main(String[] args) {
		
		Set<String> newSet = new TreeSet<String>();
		
		newSet.add(new StringBuffer("Kaviya").toString());
		newSet.add(new StringBuffer("Keerthana").toString());
		newSet.add(new StringBuffer("Keerthi").toString());
		newSet.add(new StringBuffer("Kowsi").toString());
		
		newSet.stream().forEach(x -> System.out.println(x));
		
	}

}
