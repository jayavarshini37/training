/*
Requirement
    1.Demonstrate program explaining basic add and traversal operation of linked hash set
    2.Demonstrate linked hash set to array() method in java
  
Entity
    1.LinkedSetDemo
  
Method Signature
    public <S> void addEle(LinkedHashSet<S> hashset, int size) 
    public <S> void printAll(LinkedHashSet<S> hashset) 
    public static void main(String[] args)
Jobs to be Done
    1.Create a LinkedHashSet.
    2.Add values to the LinkedHashSet
    3.Call printer method.
    4.In printer method use the stream to print each elements.
    5.Convert Set to Array
        5.1)Print the Values.
  
Pseudo Code
class LinkedSetDemo {
 	
 	static Scanner scanner = new Scanner(System.in);
 	
 	public <S> void addEle(LinkedHashSet<S> hashset, int size) {
 		System.out.println("Enter the Value");
 		for(int i=0;i<=size;i++) {
 			String val = scanner.nextLine();
 			hashset.add((S) val); 
 		}
 	}
 	
 	public <S> void printAll(LinkedHashSet<S> hashset) {
 		System.out.println("Set Values");
 		hashset.stream().forEach(x -> System.out.println(x));
  	}
 	
 	public static void main(String[] args) {
 		
 		LinkedSetDemo set = new LinkedSetDemo();
 		
 		LinkedHashSet<String> hashset = new LinkedHashSet<String>();
 		System.out.println("Enter the Size of the List");
 		Integer size = scanner.nextInt();
 		set.addEle(hashset,size);
 		set.printAll(hashset);
 		}
 		
  	}

 }
  
 
 */
package com.java.training.setandmap;

import java.util.LinkedHashSet;
import java.util.Scanner;

public class LinkedSetDemo {
	
	static Scanner scanner = new Scanner(System.in);
	public <S> void addEle(LinkedHashSet<S> hashset, int size) {
		System.out.println("Enter the Value");
		for(int i=0;i<=size;i++) {
			scanner.nextLine();
		}
	}

	public <S> void printAll(LinkedHashSet<S> hashset) {
		System.out.println("Set Values");
		hashset.stream().forEach(x -> System.out.println(x));
	}
	
	public static void main(String[] args) {
		
		LinkedSetDemo set = new LinkedSetDemo();
		
		LinkedHashSet<String> hashset = new LinkedHashSet<String>();
		System.out.println("Enter the Size of the List");
		Integer size = scanner.nextInt();
		set.addEle(hashset,size);
		set.printAll(hashset);
		
		Object[] newArr = hashset.toArray();
		
		for(int i=0;i<newArr.length; i++) {
			System.out.println(newArr[i]);
		}
		
		
	}

}
