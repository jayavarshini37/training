/*
Requirement:
    To Given a random date, how would you find the date of the previous Friday

Entity:
    PreviousFriday

Method Signature:
     public static void main(String[] args)

Jobs to be Done:
    1.Get the input date From the user
    2.Invoke the LocalDate class and pass the parameter 
        which you get from user
    3.Using LocalDate class's object get the Previous Friday's date
        3.1 Print the result
    
Pseudo Code:

public class PreviousFridayDemo {
    
    public static void main(String[] args) {
        Scanner scanner// use scanner and get year,month,day
        LocalDate date = LocalDate.of(year,month,day);
        System.out.printf("Previous friday is:  %s,%n", date.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
    }
}
*/
package com.java.training.datetime;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;
   

public class PreviousFriday {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int year = scanner.nextInt();
        int month = scanner.nextInt();
        int day = scanner.nextInt();
        System.out.print("Enter the Year,Month,Day");
        LocalDate date = LocalDate.of(year,month,day);
        System.out.printf("Previous friday is:  %s,%n", date.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
        scanner.close();
    }
}