/*
Requirement
    To create two paths and test whether they represent same path.
     
Entity
    SamePath
     
Method Signature
    public static void main(String[] args);
      
Jobs To Done
    1.Get the two different paths as path1 and path2.
    2.Check whether the paths represents the same path.
 
Pseudo code
      class CheckPaths {
 
          public static void main(String[] args) {
              Path path1 = Paths.get("C:\\dev\\training1\\java\\exercise2");
 			   Path path2 = Paths.get("C:\\dev\\training1\\java\\exercise2");
 			   System.out.println(path1.equals(path2));
          }
      }
 */

package com.java.training.io;

import java.nio.file.Path;
import java.nio.file.Paths;

public class SamePath {

	public static void main(String[] args) {
		Path path1 = Paths.get("C:\\dev\\training1\\java\\exercise2");
		Path path2 = Paths.get("C:\\dev\\training1\\java\\exercise2");
		System.out.println(path1.equals(path2));
	}
}