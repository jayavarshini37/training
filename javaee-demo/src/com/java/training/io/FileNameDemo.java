/*
Requirement
    Get the file names of all file with specific extension in a directory.
 
Entity
  	FileNameDemo
  
Method Signature 
  	public static void main(String[] args)

Jobs To Be Done
  	1)Create a object for File with directory as argument.
  	2)Add all file names to the array.
  	3)for each file name in the array.
  		3.1)check whether the extension matches with given extension.
  			3.1.1)if matches, Print the file names.
 
PseudoCode
  
  		class FileNameDemo {
 
 			public static void main(String[] args) {
 				File file = new File("C:\\dev\\training1\\java\\exercise2");
 				File[] fileArray = file.listFiles();
 			for (File files : fileArray ) {
 					//check and print the file name which matches the given extension.
 				} 			
 			}
 		}
 */
package com.java.training.io;

import java.io.File;

public class FileNameDemo {

	public static void main(String[] args) {
		File file = new File("C:\\dev\\training1\\java\\exercise2");
		File[] fileArray = file.listFiles();
		for (File files : fileArray ) {
			if(files.getName().toLowerCase().endsWith(".txt")) {
				System.out.println(files.getName());
			}
		}
		
	}
}