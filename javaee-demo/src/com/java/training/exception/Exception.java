/*Requirement:
    To handle and give the reason for the exception in the following code

Entity:
	Exception
	
Method signature:
	public static void main(String[] args)
	
Jobs to be done:
    1. Create an array inside the try block
        1.1 Print arr[7]
        1.2 If the index is not there in created array,
            1.2.i) Print the catch block

Pseudo code:
public class Exception {  
   
	public static void main(String[] args) {
	    try {
	        int arr[] ={1,2,3,4,5};
	            System.out.println(arr[7]);
	}   catch(ArrayIndexOutOfBoundsException e) {
	        System.out.println("The specified index does not exist " + "in array");
	}
   }

*/ 
package com.java.training.exception;

public class Exception {  
   
	public static void main(String[] args) {
	    try {
	        int arr[] ={1,2,3,4,5};
	            System.out.println(arr[7]);
	}   catch(ArrayIndexOutOfBoundsException e) {
	        System.out.println("The specified index does not exist " + "in array");
	}
   }

	public char[] getMessage() {
		// TODO Auto-generated method stub
		return null;
	}
}