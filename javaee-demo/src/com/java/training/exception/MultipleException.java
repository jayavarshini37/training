/*
 Requirements:
    To Demonstrate the catching multiple exception with example. 

 Entity:
    MultipleException 
 
 Method Signature:
    public static void main(String[] args)
 
 Jobs to be done:
   1. Declare the try block.
   2. Create an array inside the try block
      2.1 Calculate 30/0 in array of index 4
      2.2 If it works correctly print statement "Last Statement of try block"
      2.3 Check the Arithmetic exception in the first catch block, 
          if it throws Arithmetic exception, print "/ by zero
      2.4 Check the ArrayIndexOutOfBoundException in the second catch block,
           if it throws the exception, print "Array elements are outside the limit
      2.5 In another catch block check for the exception e and print another exception
      2.6 Then exit form the multiple catch block and print exit the try block

Pseudo code
public class MultipleException {    
    public static void main(String[] args) {    
        try {    
            int array[] = new int[10];    
            array[10] = 30/0;    
        } catch(ArithmeticException | ArrayIndexOutOfBoundsException e) {  
            System.out.println(e.getMessage());  
        } catch(Exception e) { 
            System.out.println(e.getMessage());  
        }
    }    
}
*/

package com.java.training.exception;

public class MultipleException {    
    public static void main(String[] args) {    
        try {    
            int array[] = new int[10];    
            array[10] = 30/0;    
        } catch(ArithmeticException | ArrayIndexOutOfBoundsException e) {  
            System.out.println(e.getMessage());  
        } catch(Exception e) { 
            System.out.println(e.getMessage());  
        }
    }    
}