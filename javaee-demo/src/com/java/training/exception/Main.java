/*
Requirement:
   To Write a program ListOfNumbers (using try and catch block).
 
Entity:
    ListOfNumber
    Main
 
Method Signature:
    public void writeList()
    public static void main(String[] args)

Jobs to be done:
    1. Declare an array with size 10 
    2. Declare the method writeList()
        2.1 Assign arrayOfNumbers inside the try block
        2.2 Execute catch block for NumberFormatException
           2.2.1 Print the exception
        2.3 Execute another catch block for IndexOutOfBoundException
           2.3.1 Print the exception
        2.4 Inside the main class create an object for ListOfNumber
             and call the writeList() method.
Pseudo code
class ListOfNumber {
    public int[] arrayOfNumbers = new int[10];
    public void writeList() {
        try {
            arrayOfNumbers[10] = 11;
      } catch (NumberFormatException e1) {
            System.out.println("NumberFormatException => " + e1.getMessage());
      } catch (IndexOutOfBoundsException e2) {
            System.out.println("IndexOutOfBoundsException => " + e2.getMessage());
    }
  }
}
 */

package com.java.training.exception;

class ListOfNumber {
    public int[] arrayOfNumbers = new int[10];
    public void writeList() {
        try {
            arrayOfNumbers[10] = 11;
      } catch (NumberFormatException e1) {
            System.out.println("NumberFormatException => " + e1.getMessage());
      } catch (IndexOutOfBoundsException e2) {
            System.out.println("IndexOutOfBoundsException => " + e2.getMessage());
    }
  }
}

class Main {
  public static void main(String[] args) {
    ListOfNumber list = new ListOfNumber();
    list.writeList();
  }
}