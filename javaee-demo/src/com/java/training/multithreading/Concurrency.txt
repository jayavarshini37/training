Concurrency:

     - Concurrency is dealing with lot of things at once
    
     - Concurrency is the ability to run several programs or several parts of a program.
         If a time consuming task can be performed asynchronously , this improves the throughput 
         and the interactivity of the program. A modern computer has several CPU's or several cores within one CPU.
 
 Parallelism:
 
     - Parallelism is about doing lot of things at once, we can speed up the program.
       
     - Parallelism is when tasks literally run at the same time
         eg. ... An application can be both parallel ,
         which means that it processes multiple tasks parallely in multi-core CPU at same time .