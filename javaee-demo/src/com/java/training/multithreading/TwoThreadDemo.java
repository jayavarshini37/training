/*
 Requirements:
    Write a program of performing two task by two threads that implements Runnable interface
    
Entities:
    TwoThreadDemo
    
Method Signature:
    public static void main(String args[])
    void run()
    start()
    
Jobs to be done:
    1. Declare and define the method void run() inside the anonymous class
         by runnable interface 
    2. Declare and define the method void run() inside another anonymous class
         by runnable interface
    3. Create two objects as t2 and t2 to call the void run() method
    4. Call the method by start method
    
Pseudocode:

    class TwoThreadDemo {  
    public static void main(String args[]) {  
        Runnable r1=new Runnable() {  
        public void run() {  
            //Define the task1 ;  
    }  
  };  
  
       Runnable r2=new Runnable() {  
       public void run() {  
            //Define the task2 
    }  
  };  
		  Thread t1=new Thread(r1);  
		  Thread t2=new Thread(r2);  

       
	      t1.start();  
	      t2.start();  
 }  
}  

    
 */
package com.java.training.multithreading;

class TwoThreadDemo {  
    public static void main(String args[]) {  
        Runnable r1=new Runnable() {  
        public void run() {  
            System.out.println("task one");  
    }  
  };  
  
       Runnable r2=new Runnable() {  
       public void run() {  
            System.out.println("task two");  
    }  
  };  
		  Thread t1=new Thread(r1);  
		  Thread t2=new Thread(r2);  

       
	      t1.start();  
	      t2.start();  
 }  
}