/*
1.Requirements :
    Create a stack using generic type and implement
    Push atleast 5 elements
    Pop the peek element
    Search a element in stack and print index value
    Print the size of stack
    Print the elements using Stream
    Reverse List Using Stack with minimum 7 elements in list.

2.Entity:
    QueueDemo 
3.Method signature:
    linkedList()
    priority
    public static void main(String [] args)
4.Jobs to be done :  
    1.Create the  object for class and invoke the two methods.
    2.In the linkedList() method create Queue with generic String type 
        2.1 Create the stream for displaying the Stack elements.
        2.2 Add 5 values to the Queue using add() method and using remove() method remove the values from the queue
        2.3 Search the specified value using search() method and find size of stack using size() method.
        2.4 Using contains() method,find the given value is in the Queue or not 
        2.5 Using size() method find the  size of the queue.
    3.Print the queue values using stream and for each value.
    4.In the priority() method creating queue named PriorityQueue
        4.1 Create the stream to print the queue values.
        4.2 Add the values to queue using add() method 
        4.3 Find the  specified value using contains() method.
        4.4 Find the size using size() method 
    5.print the PriorityQueue values using stream and for each

Pseudo code
public class QueueDemo{
	public void linkedList() {
		 Queue<String> vegetables = new LinkedList<String>();
	        Stream<String> stream = vegetables.stream();
	        vegetables.add();
	        vegetables.add();
	        vegetables.add();
	        vegetables.add();
	        vegetables.add();
	        vegetables.remove();
	        
	        System.out.println(vegetables.contains());
	        System.out.println(vegetables.size());       
	        stream.forEach((element) -> {
	           System.out.println(element);  
	        }); 
	}
	public void priority() {
		 Queue<String> vegetables = new PriorityQueue<String>();
	        Stream<String> stream = vegetables.stream();
	        vegetables.add();        
	        vegetables.add();
	        vegetables.add();
	        vegetables.add();
	        vegetables.add();
	        vegetables.remove();
	        System.out.println(vegetables.contains());
	        System.out.println(vegetables.size());   
	        stream.forEach((element) -> {
	           System.out.println(element); 
	        });    //Process Stack Using Stream
	    }
    public static void main(String [] args) {
    	QueueDemo queue = new QueueDemo();
    	System.out.println("Linked List");
    	queue.linkedList();
    	System.out.println("PriorityQueue");
    	queue.priority();
    }
}

*/
package com.java.training.queue;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;
public class QueueDemo{
	public void linkedList() {
		 Queue<String> vegetables = new LinkedList<String>();
	        Stream<String> stream = vegetables.stream();
	        vegetables.add("Brinjal");
	        vegetables.add("Carrot");
	        vegetables.add("Potato");
	        vegetables.add("Cabbage");
	        vegetables.add("Bottle guard");
	        vegetables.remove();
	        
	        System.out.println(vegetables.contains("Brinjal"));
	        System.out.println(vegetables.size());       
	        stream.forEach((element) -> {
	           System.out.println(element);  
	        }); 
	}
	public void priority() {
		 Queue<String> vegetables = new PriorityQueue<String>();
	        Stream<String> stream = vegetables.stream();
	        vegetables.add("Brinjal");        
	        vegetables.add("Carrot");
	        vegetables.add("Potato");
	        vegetables.add("Cabbage");
	        vegetables.add("Bottle guard");
	        vegetables.remove();
	        System.out.println(vegetables.contains("Carrot"));
	        System.out.println(vegetables.size());   
	        stream.forEach((element) -> {
	           System.out.println(element); 
	        });    //Process Stack Using Stream
	    }
    public static void main(String [] args) {
    	QueueDemo queue = new QueueDemo();
    	System.out.println("Linked List");
    	queue.linkedList();
    	System.out.println("PriorityQueue");
    	queue.priority();
    }
}
