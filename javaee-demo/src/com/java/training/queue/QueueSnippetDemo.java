/*
1.Requirement:
    To find the output and to complete the code

2.Entity:
    QueueSnippetDemo 

3.Method signature:
    public static void main(String [] args)

4.Jobs to be done :  
    1.Create the Queue with generic String type 
        1.1 Add values using add() method.
        1.2 Printing the peek value.
        
5.Pseudo code
public class QueueSnippetDemo {
    public static void main(String[] args) {
        Queue<String> vegetables = new PriorityQueue<String>();
        vegetables.add();
        vegetables.add();
        vegetables.add();
        vegetables.add();
        vegetables.add();
        vegetables.add();
        vegetables.poll();
        System.out.println(vegetables.peek());  
    }
}
 */
package com.java.training.queue;

import java.util.PriorityQueue;
import java.util.Queue;
public class QueueSnippetDemo {
    public static void main(String [] args) {
        Queue<String> vegetables = new PriorityQueue<String>();
        vegetables.add("Carrot");
        vegetables.add("Potato");
        vegetables.add("Bitter guard");
        vegetables.add("Beans");
        vegetables.add("Snake guard");
        vegetables.add("Brinjal");
        vegetables.poll();
        System.out.println(vegetables.peek());  
    }
}