/*
Requirement
    Name
    studentId
    Address
    Phone Number

Entity:
    SerializeFile 

Method Signature:
   -none-

Jobs to be done:
    1. Create a Class name SerializeTheFile and declare main method
    2.Create a Object for Scanner name sc
    3.Get input for name,studentid,address,phonenumber and store it in  name,d,address,phonenumber.
    4.Use try
        4.1 Set the path to store ser file and store it in fileout
        4.2 Create a object for OutputStream name out for fileOut
        4.3 Write s in out
    5.Catch IOException
        5.1 call method printStackTrace()

Psudocode:

public class SerializeFile {
	   public static void main(String [] args) {
		      Scanner sc = new Scanner(System.in);
		      SerializeFile s = new SerializeFile();
		      PRINT "Name: ";
		      s.name = sc.next();
		      PRINT "Student Id: ";
		      s.studentId = sc.next();
		      PRINT "Address: ";
		      s.address = sc.next();
		      PRINT "Phone Number: ";
		      s.phonenumber = sc.nextLong();
		      TRY {
		         FileOutputStream fileOut = new FileOutputStream("E:\\student.ser.txt");
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(s);
		         out.close();
		         fileOut.close();
		         PRINT "Serialized data is saved in student.ser";
		      } CATCH IOException i {
		         i.printStackTrace();
		      }
		   }
}
 */
package com.java.training.serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class SerializeFile {
	   private String name;
	private String studentId;
	private String address;
	private long phonenumber;

	public static void main(String [] args) {
		      Scanner sc = new Scanner(System.in);
		      SerializeFile s = new SerializeFile();
		      System.out.println("Name: ");
		      s.name = sc.next();
		      System.out.println("Student Id: ");
		      s.studentId = sc.next();
		      System.out.println("Address: ");
		      s.address = sc.next();
		      System.out.println("Phone Number: ");
		      s.phonenumber = sc.nextLong();
		      try {
		         FileOutputStream fileOut = new FileOutputStream("E:\\student.ser.txt");
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(s);
		         out.close();
		         fileOut.close();
		         System.out.println("Serialized data is saved in student.ser");
		      } catch (IOException i) {
		         i.printStackTrace();
		      }
		   }
}