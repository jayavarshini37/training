/*
Requirement:
    Write a Java program to get the portion of a map whose keys range from a given key to another key.
     
Entity:
    public class PortionMap

Function Declaration:
    public static void main(String[] args)

Jobs to be Done:
    1. Create a TreeMap named treemap
        1.i) Add the elements with key in treemap
        1.ii) Print the treemap
    2. Create a SortedMap named subTreeMap
        2.i) Using subMap() method add keys from the treeMap 
        2.ii) Print the subMap
        
Pseudo code
public class PortionMap {
    
    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();
        SortedMap<Integer, String> subTreeMap = new TreeMap<>();
        treeMap.put();
        treeMap.put();
        treeMap.put();
        treeMap.put();
        treeMap.put();
        System.out.println("treemap is " + treeMap);
        subTreeMap = treeMap.subMap(10,30);
        System.out.println("range is " + subTreeMap);
    }

}

 */
package com.java.training.map;

import java.util.SortedMap;
import java.util.TreeMap;

public class PortionMap {
    
    public static void main(String[] args) {
        TreeMap<Integer, String> treeMap = new TreeMap<>();
        SortedMap<Integer, String> subTreeMap = new TreeMap<>();
        treeMap.put(10, "Flute");
        treeMap.put(20, "Violin");
        treeMap.put(30, "Keys");
        treeMap.put(40, "Guitar");
        treeMap.put(50, "Veena");
        System.out.println("treemap is " + treeMap);
        subTreeMap = treeMap.subMap(10,30);
        System.out.println("range is " + subTreeMap);
    }

}
