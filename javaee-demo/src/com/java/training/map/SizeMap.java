/*
Requirement:
    To write the java program to count size of mappings.

Entity:
    public class Sizemap

Method Signature:
    
    public static void main(String[] args)
    size()

Jobs to be done:
    1. Create the HashMap and the elements with key
       1.1 Print the hashMap elements with key and name
       1.2 Using size method, print the size of the mapping 
       
Pseudo code
public class SizeMap {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put();
        hashMap.put();
        hashMap.put();
        hashMap.put();
        hashMap.put();
        hashMap.put();
        hashMap.put();
        hashMap.put();
        System.out.println("Key and Name" + hashMap);
        System.out.println("Size of the mappings " + hashMap.size());
    }
   
}
*/
package com.java.training.map;

import java.util.HashMap;

public class SizeMap {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "HP");
        hashMap.put(2, "Dell");
        hashMap.put(3, "Asus");
        hashMap.put(4, "Lenovo");
        hashMap.put(5, "Acer");
        hashMap.put(6, "Samsung");
        hashMap.put(8, "Avita");
        hashMap.put(7, "Huawei");
        System.out.println("Key and Name" + hashMap);
        System.out.println("Size of the mappings " + hashMap.size());
    }
   
}