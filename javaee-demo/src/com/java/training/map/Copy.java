
/*Requirement:
	To write the java program to copy all of the mappings from the specified map to another map

Entity:
	public class Copy
	
Method Signature:
	public static void main(String[] args)

Jobs to be done:
	1. Create HashMap named map1 and add the elements with key
        1.i) Print the elements with key
    2. Create HashMap named map2 and add the elements with key
        2.i) Print the elements with key
    3. Using putAll() method put the elements from the map2 to map1
        3.i) Print the elements in map1 with key.

Pseudo code
public class Copy {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap1 = new HashMap<>();
        hashMap1.put();
        hashMap1.put();
        hashMap1.put();
        System.out.println("Values of hashMap1 are" + " " + hashMap1);
        
        HashMap<Integer, String> hashMap2 = new HashMap<>();
        hashMap2.put();
        hashMap2.put();
        hashMap2.put();
        System.out.println("Value of hashMap2 are" + " " + hashMap2);
        
        hashMap2.putAll(hashMap1);
        System.out.println("The value of hashMap2 are" + " " + hashMap2);
    }

}

*/
package com.java.training.map;

import java.util.HashMap;

public class Copy {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap1 = new HashMap<>();
        hashMap1.put(1, "French fries");
        hashMap1.put(2, "American chop suey");
        hashMap1.put(3, "Mutton balls");
        System.out.println("Values of hashMap1 are" + " " + hashMap1);
        
        HashMap<Integer, String> hashMap2 = new HashMap<>();
        hashMap2.put(4, "Briyani");
        hashMap2.put(5, "Pasta");
        hashMap2.put(6, "Fish curry");
        System.out.println("Value of hashMap2 are" + " " + hashMap2);
        
        hashMap2.putAll(hashMap1);
        System.out.println("The value of hashMap2 are" + " " + hashMap2);
    }

}
