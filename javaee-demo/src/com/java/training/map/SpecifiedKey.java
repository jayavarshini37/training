 /*
Requirement:
    To write a Java program to test if a map contains a mapping for the specified key.

Entity:
    public class SpecifiedKey
    
Method Signature:
    public static void main(String[] args)

Jobs to be Done:
    1. Create a HashMap named hashMap and add the elements into it
    2. Using boolean check whether the key is present in the hashMap
        2.1 if it is present in the HashMap 
             2.1.i) print Key presents in the hashMap is true
        2.2 if it is not present in the HashMap
             2.2.ii) print Key presents in the hashMap is false
             
Pseudo code
public class SpecifiedKey {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put();
        hashMap.put();
        hashMap.put();
        hashMap.put();
        hashMap.put();
        boolean key = hashMap.containsKey(11);
        System.out.println("The key presents in the hashmap" + " " + key);
        
        boolean key1 = hashMap.containsKey(20);
        System.out.println( "The key presents in the hashmap" + " " + key1);
    }

}
*/
package com.java.training.map;

import java.util.HashMap;

public class SpecifiedKey {
    
    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(20, "Kerala");
        hashMap.put(21, "Bangaluru");
        hashMap.put(22, "Uttarpradesh");
        hashMap.put(23, "Rajasthan");
        hashMap.put(24, "Tamilnadu");
        boolean key = hashMap.containsKey(11);
        System.out.println("The key presents in the hashmap" + " " + key);
        
        boolean key1 = hashMap.containsKey(20);
        System.out.println( "The key presents in the hashmap" + " " + key1);
    }

}