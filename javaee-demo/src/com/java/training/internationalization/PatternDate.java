/*1.Requirement:
     - Program to print the following pattern of the Date and Time using SimpleDateFormat.
           "yyyy-MM-dd HH:mm:ssZ"
     
2.Entity:
     - PatternDate
     
3.Function Declaration:
  	 - public static void main(String[] args)
  	 
4.JobsToBeDone:
  	1.Store date pattern in pattern String.
  	2.Create SimpleDateFormat class pass argument pattern.
  	3.Get pattern format using invoke format method.
  	4.Print the format date.      
    
PseudoCode:

public class PatternDate {
	public static void main(String[] args) {
		String pattern = "yyyy-MM-dd HH:mm:ssZ";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		String date = simpleDateFormat.format(new Date(0));
		System.out.println(date);
	}
}

*/ 
package com.java.training.internationalization;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class PatternDate {
	public static void main(String[] args) {
		String pattern = "yyyy-MM-dd HH:mm:ssZ";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		String date = simpleDateFormat.format(new Date(0));
		System.out.println(date);
	}

}