/*
Requirement:
    Program to change the number format to Denmark number format.
     
Entity:
    NumberFormatDenmark
     
Function Declaration:
  	public static void main(String[] args)
  	 
Jobs To Be Done:
  	1.Invoke the NumberFormat class getInstance method parameter as 
create Locale class with argument Denmark number format.
    2.Print the Denmark number format.
    
PseudoCode:

import java.net.*;
public class NumberFormatDenmark {
	public static void main(String[] args) {
		System.out.println(NumberFormat.getInstance(new Locale("Denmark"))
				                       .format(32124));
	}
}
-
*/ 
package com.java.training.internationalization;
import java.text.NumberFormat;
import java.util.Locale;

public class NumberFormatDenmark {
	public static void main(String[] args) {
		System.out.println("Denmark Number Foramt:-");
		System.out.println(NumberFormat.getInstance(new Locale("Denmark"))
				                       .format(32124));

	}

}