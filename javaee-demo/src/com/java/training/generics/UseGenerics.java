
/*
Requirement
  To find the output of the program

Entity:
  1.UseGenerics
  2.MyGen 
  
Methods Signature:
  1.void set(T var);
  2.T get();
 
Pseudocode
public class UseGenerics {
	
	public static void main(String[] args) {  
	    }
}

class MyGen<T>
{
    T var;
    void  set(T var)
    {
        this.var = var;
    }
    T get()
    {
        return var;
    }

}
Answer
    No This Program will not execute and give output since the Generic object created for MyGen class is of Integer type 
      and the set method of MyGen Class takes the Integer type data, but the input gave is of String type, so the Compiler shows 
        error in the Compile Time..  
 
 */
package com.java.training.generics;

public class UseGenerics {
	
	public static void main(String[] args) {  
		/*
        MyGen<Integer> m = new MyGen<Integer>();  
        m.set("merit");
        System.out.println(m.get());*/
    }
}

class MyGen<T>
{
    T var;
    void  set(T var)
    {
        this.var = var;
    }
    T get()
    {
        return var;
    }

}