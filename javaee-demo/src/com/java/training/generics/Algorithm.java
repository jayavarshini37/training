/*
Requirement:
    Will the following class compile? If not, why?
        public final class Algorithm {
            public static <T> T max(T x, T y) {
                return x > y ? x : y;
    		}
 		}
 
Entity:
    Algorithm
  
Method Signature:
    public static <T> T max(T x , T y);

Answer:
  1.No,The Following Class will not Compiler since the parameter and the return type is Generic Object. 
  Since the Generic Object can't be predicted to compared using unary and binary operators.
 */
package com.java.training.generics;

public final class Algorithm {

    public static <T> T max(T x, T y) {
        return x > y ? x : y;
    }
	
}