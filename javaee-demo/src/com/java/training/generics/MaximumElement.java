
/*
Requirement
    To Write a generic method to find the maximal element in the range [begin, end) of a list.

Entity:
    MaxElement  

Method Signature:
    public <S> int maximumReturner(List<S> list, int start, int end);
    public static void main(String[] args)

Jobs to be Done:
  1.Create a List and add elements.
  2.Get the range from the user.
  3.Pass the List and two values to the maximumReturner method created. 
  
Pseudo code
public class MaximumElement {
	
	public <S> int maximumReturner(List<S> list, int start, int end) {
		int count = 1;
		
		List<S> secList = new ArrayList<S>();
		
		for(int i=start; i<=end; i++) {
			secList.add(list.get(i));
		}
		
		
		Set<S> newSet = new HashSet<S>(secList);
		
		for(S i: newSet) {
			count = Collections.frequency(list, i);
		}
		
		return count;
	}
	
	public static void main(String[] args) {
		try (Scanner scanner = new Scanner(System.in)) {
			MaximumElement range = new MaximumElement();
			List<Integer> newList = new ArrayList<Integer>();
			
			newList.add();
			newList.add();
			newList.add();
			newList.add();
			newList.add();
			newList.add();
			newList.add();
			newList.add();
			newList.add();
			newList.add();
			newList.add();
			
		    System.out.println("Enter the Range");
			int startVal = scanner.nextInt();
			int endVal = scanner.nextInt();
			
			System.out.println(range.maximumReturner(newList, startVal, endVal));
		}
		
	}

}
*/ 
package com.java.training.generics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class MaximumElement {
	
	public <S> int maximumReturner(List<S> list, int start, int end) {
		int count = 1;
		
		List<S> secList = new ArrayList<S>();
		
		for(int i=start; i<=end; i++) {
			secList.add(list.get(i));
		}
		
		
		Set<S> newSet = new HashSet<S>(secList);
		
		for(S i: newSet) {
			count = Collections.frequency(list, i);
		}
		
		return count;
	}
	
	public static void main(String[] args) {
		try (Scanner scanner = new Scanner(System.in)) {
			MaximumElement range = new MaximumElement();
			List<Integer> newList = new ArrayList<Integer>();
			
			newList.add(3);
			newList.add(2);
			newList.add(1);
			newList.add(3);
			newList.add(3);
			newList.add(3);
			newList.add(3);
			newList.add(3);
			newList.add(1);
			newList.add(1);
			newList.add(3);
			
		    System.out.println("Enter the Range");
			int startVal = scanner.nextInt();
			int endVal = scanner.nextInt();
			
			System.out.println(range.maximumReturner(newList, startVal, endVal));
		}
		
	}

}