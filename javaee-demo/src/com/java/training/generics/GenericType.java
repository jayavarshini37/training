/*
Requirement:
    Write a program to demonstrate Generic - for loop, for list, set and map.
  
Entity
    1.GenericList Class
    2.GenericSet Class
    3.GenericMap Class
    4.GenericTypes Class
  
Method Signature:
    public void addEle(S val)
    public void remEle(S val)
    public static void main(String[] args) 
  
Jobs to be done:
    1.Create a new List of Generic type called list.
        1.1 Create a new method of void type called addEle that takes a value to add the value to list.
        1.2 Create another method called remEle that takes a value to be removed from the list
    2.Create another Class for Set called Generic Set.
        2.1 Create a new Generic Set called newSet.
        2.2 Create a method that takes a value and returns void, this method add the value to the Set.
        2.3 Another method is created that takes to remove from the Set and returns void.
    3.Create another Class for Map and called as Generic Map
        3.1 Create a new Generic Map which takes two parameters as key and value.
        3.2 Create a void method that takes two Generic type as parameter and adds to the Map with the Key
          and Value.
        3.3 Create another void method that takes two Genetic type as parameter and removes the specific 
          value form the Map with the value and key provided.
        3.4 Create a new Object of Generic List of Integer type 
  	    3.5 Add values to the List by the addEle method of the GenericList.
    4.Print the Values from the List with the For each Loop and printing it to the Console.
    5.Create a new Object of Generic Set of String type 
  	    5.1  add values to the List by the addSetEle method of the GenericSet.
        5.2 Print the Values from the Set with the For each Loop and printing it to the Console.
    6.Create a new Object of Generic Map of Integer and String type 
	    6.1 Add values to the Map by the addSetEle method of the GenericMap.
        6.2 Print the Values from the Map with the Iterator Class Object and checking the Map for the 
          next Value if the values is exist then value is printed using the entryKey and entryValue method 
          
Pseudo code
class GenericList<S> {
	
	public GenericList () {
		
	}
	public List<S> list = new ArrayList<S>();
	
	public void addEle(S val) {
		list.add(val);
	}
	
	public void remEle(S val) {
		list.remove(val);
	}
	
}

class GenericSet<S> {
	
	public Set<S> newSet = new HashSet<S>();
	
	public GenericSet () {
		
	}
	
	public void addSetEle(S val) {
		newSet.add(val);
	}
	
	public void remSetEle(S val) {
		newSet.remove(val);
	}
	
}

class GenericMap<S,K> {
	
	public Map<S,K> newMap = new HashMap<S,K>();
	
	public GenericMap() {
		
	}
	
	public void addMapEle(S pos, K val) {
		newMap.put(pos,val);
	}
	
	public void remMapEle(S pos, K val) {
		newMap.remove(pos, val);
	}
}

public class GenericType {
	
	public static void main(String[] args) {
		
		//Generic List
		GenericList<Integer> geneType = new GenericList<Integer>();

		geneType.addEle();
		geneType.addEle();
		geneType.addEle();
		geneType.addEle();
		//Printing the Elements
		System.out.println("Printing Elements using For Loop");
		for(Integer s : geneType.list) {
			System.out.println(s);
		}
		
		//Generic Set
		GenericSet<String> geneSet = new GenericSet<String>();
		
		geneSet.addSetEle();
		geneSet.addSetEle();
		geneSet.addSetEle();
		geneSet.addSetEle();
		
		System.out.println("Printing the Set Elements using for each Loop");
		
		for(String e : geneSet.newSet) {
			System.out.println(e);
		}
		
		
		//Generic Map
		GenericMap<Integer,String> geneMap = new GenericMap<Integer,String>();
		
		geneMap.addMapEle();
		geneMap.addMapEle();
		geneMap.addMapEle();
		geneMap.addMapEle();
		
		Iterator<Map.Entry<Integer,String>> iterator = geneMap.newMap.entrySet().iterator();
		
		while(iterator.hasNext()) {
			Map.Entry<Integer, String> entry = iterator.next();
			System.out.println(entry.getKey() +" "+entry.getValue());
		}
		
		
	}
	
}

 */
package com.java.training.generics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

class GenericList<S> {
	
	public GenericList () {
		
	}
	public List<S> list = new ArrayList<S>();
	
	public void addEle(S val) {
		list.add(val);
	}
	
	public void remEle(S val) {
		list.remove(val);
	}
	
}

class GenericSet<S> {
	
	public Set<S> newSet = new HashSet<S>();
	
	public GenericSet () {
		
	}
	
	public void addSetEle(S val) {
		newSet.add(val);
	}
	
	public void remSetEle(S val) {
		newSet.remove(val);
	}
	
}

class GenericMap<S,K> {
	
	public Map<S,K> newMap = new HashMap<S,K>();
	
	public GenericMap() {
		
	}
	
	public void addMapEle(S pos, K val) {
		newMap.put(pos,val);
	}
	
	public void remMapEle(S pos, K val) {
		newMap.remove(pos, val);
	}
}

public class GenericType {
	
	public static void main(String[] args) {
		
		//Generic List
		GenericList<Integer> geneType = new GenericList<Integer>();

		geneType.addEle(10);
		geneType.addEle(20);
		geneType.addEle(30);
		geneType.addEle(40);
		//Printing the Elements
		System.out.println("Printing Elements using For Loop");
		for(Integer s : geneType.list) {
			System.out.println(s);
		}
		
		//Generic Set
		GenericSet<String> geneSet = new GenericSet<String>();
		
		geneSet.addSetEle("Anu");
		geneSet.addSetEle("Abi");
		geneSet.addSetEle("Akshaya");
		geneSet.addSetEle("Aarthi");
		
		System.out.println("Printing the Set Elements using for each Loop");
		
		for(String e : geneSet.newSet) {
			System.out.println(e);
		}
		
		
		//Generic Map
		GenericMap<Integer,String> geneMap = new GenericMap<Integer,String>();
		
		geneMap.addMapEle(1,"Anu");
		geneMap.addMapEle(2,"Abi");
		geneMap.addMapEle(3,"Akshaya");
		geneMap.addMapEle(4,"Aarthi");
		
		Iterator<Map.Entry<Integer,String>> iterator = geneMap.newMap.entrySet().iterator();
		
		while(iterator.hasNext()) {
			Map.Entry<Integer, String> entry = iterator.next();
			System.out.println(entry.getKey() +" "+entry.getValue());
		}
		
		
	}
	
}
