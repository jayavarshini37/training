/*
Requirements
    Write a generic method to count the number of elements in a collection that have a specific
      property (for example, odd integers, prime numbers, Palindrome).
   
Entity:
   GenericMethods
  
Method Signature:
   <S> void genericMethod(List<S> list) 
   public static void main(String[] args)
  
Jobs to be Done.
   1.Create a Generic Method which takes Generic List as Parameter.
  	  1.1)By using stream iterate the List and print the values.
   2.Create a Generic List and add values.
     2.1)Pass the List to the Generic method created. 
     
Pseudo code
public class GenericMethods {
	
	<S> void genericMethod(List<S> list) {
		list.stream().forEach(x -> System.out.println(x));
	}
	
	public GenericMethods() {
		
	}
	
	public static void main(String[] args) {
		List<Integer> newList = new ArrayList<Integer>();
		
		newList.add();
		newList.add();
		newList.add();
		newList.add();
		
		GenericMethods generic = new GenericMethods();
		
		generic.genericMethod(newList);
		
	}

}
 */
package com.java.training.generics;

import java.util.ArrayList;
import java.util.List;

public class GenericMethods {
	
	<S> void genericMethod(List<S> list) {
		list.stream().forEach(x -> System.out.println(x));
	}
	
	public GenericMethods() {
		
	}
	
	public static void main(String[] args) {
		List<Integer> newList = new ArrayList<Integer>();
		
		newList.add(10);
		newList.add(20);
		newList.add(30);
		newList.add(40);
		
		GenericMethods generic = new GenericMethods();
		
		generic.genericMethod(newList);
		
	}

}