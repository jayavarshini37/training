
/*
Requirements:
    To write a program to print employees name list by implementing iterable interface.
Entities:
  	MyIterable
  	IterableInterface
Method Signature:
  	public MyIterable(T[] t),
  	public Iterator<T> iterator().
  
Jobs To Be Done:
    1.Create the MyIterable class that implements the Iterable.
    2.Create the local variable list as generic type.
    3.Create a method MyIterable of generic type  and assigning already defined String values from the main class.
  	4.Create the main class as IterableInterface .
    5.Declare the String values in the name variable.
    6.Create the object myList for the variables to be iterated.
    7.After assigning the variables in the list by iterator implement, the final result is printed.
    
Pseudo code
class MyIterable<T> implements Iterable<T> {
	private List<T> list;
	public MyIterable(T[] t) {
		list = Arrays.asList(t);
	}
	public Iterator<T> iterator() {
		return list.iterator();
	}
}
public class IterableInterface {

	public static void main(String[] args) {
		String[] apps = {" "};
		MyIterable<String> app = new MyIterable<>(apps);
		for (String string : app) {
			System.out.println(string);
		}
	}
}

  		
2.Why except iterator() method in Iterable interface, are not necessary to define in the implemented class?
    The Iterator method is necessary to be overridden from the Iterable interface to the implementing Class because the iterator 
      method makes the Class or the custom data type iterable but the rest of the methods such as spliterator does not makes the class iterable.
  	
*/
package com.java.training.generics;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class MyIterable<T> implements Iterable<T> {
	private List<T> list;
	public MyIterable(T[] t) {
		list = Arrays.asList(t);
	}
	public Iterator<T> iterator() {
		return list.iterator();
	}
}
public class IterableInterface {

	public static void main(String[] args) {
		String[] apps = {"HP", "Lenova", "Dell", "Apple", "Microsoft"};
		MyIterable<String> app = new MyIterable<>(apps);
		for (String string : app) {
			System.out.println(string);
		}
	}
}



