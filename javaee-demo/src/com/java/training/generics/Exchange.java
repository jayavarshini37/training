/*
Requirement:
    Write a generic method to exchange the positions of two different elements in an array.

 Entity
    Exchange
 
 Method Signature
    public <S> S void swap(List<S> list, S value1, S value2);
 
 Jobs to be Done
    1.Create a Generic method which takes a List and 2 values as parameter
 	    1.1)Swap the two values of the List.
    2.Create a Generic List.
        2.1)Add Values to the List.
    3.Pass the List and two index of the value to swap method.
    4.Print the List.
 
Pseudo code
public class Exchange {
	
	public <S> List<S> swapped(List<S> list, int value1, int value2) {
		Collections.swap(list, value1, value2);
		return list;
    }
	
	public static void main(String[] args ) {
		List<Integer> newList = new ArrayList<Integer>();	
		newList.add();
		newList.add();
		newList.add();
		newList.add();
		newList.add();
		newList.add();
		newList.add();
		newList.add();
		
		Exchange Exchange = new Exchange();
		System.out.println(Exchange.swapped(newList, 1, 4));
	}

}
*/
package com.java.training.generics;
public class Exchange {
	
	public <S> List<S> swapped(List<S> list, int value1, int value2) {
		Collections.swap(list, value1, value2);
		return list;
    }
	
	public static void main(String[] args ) {
		List<Integer> newList = new ArrayList<Integer>();	
		newList.add(10);
		newList.add(20);
		newList.add(30);
		newList.add(40);
		newList.add(50);
		newList.add(60);
		newList.add(70);
		newList.add(80);
		
		Exchange Exchange = new Exchange();
		System.out.println(Exchange.swapped(newList, 1, 4));
	}

}