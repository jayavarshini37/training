/*1.Requirement:
   - Program to add 5 elements to a xml file and print the elements
in the xml file using list. Remove the 3rd element from the xml file
and print the xml file.

2.Entity:
   - PropertyStoreToXml

3.Method Signature:
   - public static List<String> toList(Set<Object> set
   - public static void main(String[] args)

4.Jobs to be done:

    1.Create a object properties of Properties class.
         1.2)Set some properties to properties object.
    2.Create a new XML file.
         2.2)Storing properties to XML file.
    4.Create a new object newProperties of Properties class.
         4.1)Loading data from XML file.
         4.2)Remove a specified property from properties.
         4.3)Display all the properties in newProperties object.
Pseudo Code:

public class PropertyStoreToXml {
    public static List toList(Set set) {
        List list = new ArrayList();
        for (Object element : set) {
            list.add(element);
        } return list;
    }
    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();
        properties.setProperty(key, value);
        properties.setProperty(key, value);
        properties.setProperty(key, value);
        FileOutputStream newFile = new FileOutputStream("C:\Users\Jayavarshini\AppData\Local\Temp\Rar$DIa24224.21345\Info.xml");
        properties.storeToXML(newFile, "Information about the project path");
        FileInputStream existingFile = new FileInputStream("C:\Users\Jayavarshini\AppData\Local\Temp\Rar$DIa24224.21345\Info.xml");
        Properties newProperties = new Properties();
        newProperties.loadFromXML(existingFile);
        List<String> keysList = toList(newProperties.keySet());
        keysList.remove(key);
    }

}

*/
package com.java.training.properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

public class PropertyStoreToXml {

    // toList method to convert Set elements to list
    public static List<String> toList(Set<Object> set) {
        List<String> list = new ArrayList<>();
        for (Object element : set) {
            list.add((String) element);
        } return list;
    }

    public static void main(String[] args) throws IOException {

        // setting to some properties to properties object of Properties
        Properties properties = new Properties();
        properties.setProperty("path", "JavaEE-Demo\\properties");
        properties.setProperty("project", "JavaEE-Demo");
        properties.setProperty("password", "1234");
        properties.setProperty("resource", "https://java jenkov.com");
        properties.setProperty("username", "Jenkov");

        //Create a XML file
        FileOutputStream newFile = new FileOutputStream("C:\\Users\\Jayavarshini\\AppData\\Local\\Temp\\Rar$DIa24224.21345\\Info.xml");

        //Store properties to xml file
        properties.storeToXML(newFile, "Information about the project path");

        //Import a XML file
        FileInputStream existingFile = new FileInputStream("C:\\Users\\Jayavarshini\\AppData\\Local\\Temp\\Rar$DIa24224.21345\\Info.xml");

        //Creating a newProperties object of Property class
        Properties newProperties = new Properties();

        //Loading properties from XML file to newProperties object
        newProperties.loadFromXML(existingFile);

        //Converting set to list
        List<String> keysList = toList(newProperties.keySet());

        for (String key : keysList) {
            System.out.println(key + " -> " + newProperties.getProperty(key));
        }

        //Removing a key from keysList
        keysList.remove("resource");

        System.out.println();

        for (String key : keysList) {
            System.out.println(key + " -> " + newProperties.getProperty(key));
        }
    }

}