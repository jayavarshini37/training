/*
 Requirement:
     To create a set and to add 10 elements
     To perform addAll() and removeAll() to the set
     To iterate the set using iterator and for each
     To explain the working of contains() and isEmpty()
    
 Entity:
     SetDemo

Method Signature:
     public static void main(String[] args)
     addAll()
     removeAll()
     contains()
     isEmpty()

Jobs to be done:
    1.Create a set and add 10 values in it
        1.i)Print the set
    2.Create another set
        2.i)Using addAll method,add all the elements into the first set
        2.ii)Print the set
            2.1.i)Using removeAll method,remove the added elements from the set
            2.1.ii)Print the set
    3.Display the set elements using iterator interface
    4.Display the set elements using for each
    5.Using contains method,check whether the element is present or not
        5.i)If it is present,print true
    6.Using isEmpty method,check whether the set is empty or not
        6.i)If it is empty,print true.

Pseudo code
public class SetDemo {

	public static void main(String[] args) {
		
		//Creating a set and adding 10 elements to it.
		Set<String> places = new HashSet<>();
		places.add();
		places.add();
		places.add();
		places.add();
		places.add();
		places.add();
		places.add();
		places.add();
		places.add();
		places.add();
        System.out.println("Creating a set and adding 10 elements to it:\n" + places );
        
        //Creating another set with some elements.
        Set<String> newPlaces = new HashSet<>();
        newPlaces.add();
        newPlaces.add();
        newPlaces.add();
        newPlaces.add();
        
        //Perform addAll() method and removeAll() to the set.
        places.addAll(newPlaces);
        System.out.println("The set is ");
        System.out.println(places);
        System.out.println("The set is ");
        places.removeAll(newPlaces);
        System.out.println(places);
        newPlaces.clear();
        
        
      //Displaying all the set elements using Iterator interface
        Iterator<String> place = places.iterator();
        System.out.println("The set is ");
        while (place.hasNext()) {
            System.out.println(place.next());
        }

        //Displaying all the set elements using ForEach
        System.out.println("The set is ");
        places.forEach(System.out::println);

        //contains()
        System.out.println("\t------------------contains() method---------------------");
        //Print the result

        //isEmpty()
        System.out.println("\t------------------isEmpty() method---------------------");
        //Print the result
	}

}
*/
package com.java.training.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {

	public static void main(String[] args) {
		
		//Creating a set and adding 10 elements to it.
		Set<String> places = new HashSet<>();
		places.add("India");
		places.add("Pakistan");
		places.add("Austria");
		places.add("Bahrain");
		places.add("Cuba");
		places.add("Denmark");
		places.add("Egypt");
		places.add("Fiji");
		places.add("Germany");
		places.add("Kenya");
        System.out.println("Creating a set and adding 10 elements to it:\n" + places );
        
        //Creating another set with some elements.
        Set<String> newPlaces = new HashSet<>();
        newPlaces.add("Israel");
        newPlaces.add("Malaysia");
        newPlaces.add("Norway");
        newPlaces.add("Russia");
        
        //Perform addAll() method and removeAll() to the set.
        places.addAll(newPlaces);
        System.out.println("The set is ");
        System.out.println(places);
        System.out.println("The set is ");
        places.removeAll(newPlaces);
        System.out.println(places);
        newPlaces.clear();
        
        
      //Displaying all the set elements using Iterator interface
        Iterator<String> place = places.iterator();
        System.out.println("The set is ");
        while (place.hasNext()) {
            System.out.println(place.next());
        }

        //Displaying all the set elements using ForEach
        System.out.println("The set is ");
        places.forEach(System.out::println);

        //contains()
        System.out.println("\t------------------contains() method---------------------");
        System.out.println("Checks place set contains Austria " + places.contains("Tata Motors"));

        //isEmpty()
        System.out.println("\t------------------isEmpty() method---------------------");
        System.out.println("Checks newPlaces is empty or not? " + newPlaces.isEmpty());
	}

}