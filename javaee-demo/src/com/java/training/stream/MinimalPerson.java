
/*
Requirement:
    To print minimal person with name and email address from the Person class using java.util.Stream<T> API by referring Person.java
  
Entity:
  	MinimalPerson
 
Method Signature:
 	public static <R, T> void main(String[] args)
 
Jobs To Be Done:
  	1) Access the Predefined roster list as a name roster.
  	2) Using for loop, the name and mail address from the roster list is added to the name and mailId list respectively.
  	3) Finding the minimal of name
  	4) Printing the Minimal Person name and mailId. 

Pseudo code
public class MinimalPerson {
	
	public static <R, T> void main(String[] args) {
		List<Person> roster = Person.createRoster();
		ArrayList<String> name = new ArrayList<>();
		ArrayList<String> mailId = new ArrayList<>();
		
		for (Person p : roster) {
			name.add(p.getName());
			mailId.add(p.getEmailAddress());
		}
		
		String minimalName = Collections.min(name);
		String minimalId = Collections.min(mailId);

		System.out.println(
				"The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
	}

}

 */
package com.java.training.stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MinimalPerson {
	
	public static <R, T> void main(String[] args) {
		List<Person> roster = Person.createRoster();
		ArrayList<String> name = new ArrayList<>();
		ArrayList<String> mailId = new ArrayList<>();
		
		for (Person p : roster) {
			name.add(p.getName());
			mailId.add(p.getEmailAddress());
		}
		
		String minimalName = Collections.min(name);
		String minimalId = Collections.min(mailId);

		System.out.println(
				"The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
	}

}
