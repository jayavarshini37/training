/*
Requirement:
    To Write a program to filter the Person, who are male and age greater than 21
  
Entity:
  	GenderAge
  
Method Signature:
   	public static void main(String[] args)
  
Jobs To Be Done:
    1. Access the precreated  roaster list referred from Person.java.
    2. Iterate every element in the Person and check for,
        2.1) person's gender is male and
        2.2) person's age is greater than 21
        2.3) Print those person's name. 

Pseudo code
public class GenderAge {
    
    public static void main(String[] args) {
    	List<Person> roster = Person.createRoster();  
    
    	for (Person p : roster) {
    	    if (p.getGender() == Person.Sex.MALE ) {
    	    	if(p.getAge() > 21) {
    	        System.out.print(p.getName() + " ");
    	    	}
    	    }
   
	    }
    }
}
*/
package com.java.training.stream;

import java.util.List;

public class GenderAge {
    
    public static void main(String[] args) {
    	List<Person> roster = Person.createRoster();  
    
    	for (Person p : roster) {
    	    if (p.getGender() == Person.Sex.MALE ) {
    	    	if(p.getAge() > 21) {
    	        System.out.print(p.getName() + " ");
    	    	}
    	    }
   
	    }
    }
}