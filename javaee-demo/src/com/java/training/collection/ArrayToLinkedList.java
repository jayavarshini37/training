
/* 
Requirement
    Create an array list with 7 elements, and create an empty linked list add all elements of 
      the array list to linked list ,traverse the elements and display the result
 
Entity
    ArrayToLinkedList

Method Signature
    public static void main(String [] args) 

Jobs to be done
    1.Create a array list 
    2.Add 7 values in the list
    3.Create linked list 
    4.Add all values in array list to linked list
    5.Using iterator print all values

Pseudo Code:
public class ArrayToLinkedList {
	public static void main(String[] args) {
		List<String> color = new ArrayList<String>();
		color.add(" "); 	//Add 7 values in the list
		
		List<String> colors = new LinkedList<String>();
        while(color.size() > 0) {
            colors.add(color.remove(0));
            }
		
        Iterator<String> iter = colors.listIterator(); 
       
        while(iter.hasNext()){ 
           System.out.println(iter.next()); 
*
*/
package com.java.training.collection;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;

public class ArrayToLinkedList {
	public static void main(String[] args) {
		List<String> color = new ArrayList<String>();
		color.add("Yellow"); 	//Add 7 values in the list
		color.add("Browm");
		color.add("Black");
		color.add("Red");
		color.add("Orange");
		color.add("White");
		color.add("Green");
		System.out.println("Elements in the array List : "+ color);
		List<String> colors = new LinkedList<String>();
        while(color.size() > 0) {
        	colors.add(color.remove(0));
            }
		System.out.println("Elements in the Linked List : "+ colors);
        Iterator<String> iter = colors.listIterator(4); 
        System.out.println("After Traverse the elements using Iterator:"); 
        while(iter.hasNext()){ 
           System.out.println(iter.next()); 
        } 
	}

}
