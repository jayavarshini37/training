
/* 
Requirement
    To achieve the concepts of Addition,Substraction,Multiplication and Division 
      using Lambda expression and functional interface.

Entity
    LambdaExp
    Arthmetic - Interface 

Method Signature
    public static void main(String[] args)
    int operation(int a, int b)

Jobs to be done
    1.Create a interface named Arithmetic
    2.Put method int operation(int a, int b)
    3.In the main function,Addition,Substraction,Multiplication and Division concepts are 
        achieved using Lambda expression 
    4.Print the Values

Pseudo Code
interface Arithmetic {
	int operation(int a, int b);
}

public class LambdaExp {
	public static void main(String[] args) {

		Arithmetic addition = (int a, int b) -> (a + b);
		
		Arithmetic subtraction = (int a, int b) -> (a - b);
		
		Arithmetic multiplication = (int a, int b) -> (a * b);
		
		Arithmetic division = (int a, int b) -> (a / b);
		
  */
package com.java.training.collection;

interface Arithmetic {
	int operation(int a, int b);
}

public class LambdaExp {
	public static void main(String[] args) {

		Arithmetic addition = (int a, int b) -> (a + b);
		System.out.println("Addition = " + addition.operation(1,2));
		Arithmetic subtraction = (int a, int b) -> (a - b);
		System.out.println("Subtraction = " + subtraction.operation(10,5));
		Arithmetic multiplication = (int a, int b) -> (a * b);
		System.out.println("Multiplication = " + multiplication.operation(10,6));
		Arithmetic division = (int a, int b) -> (a / b);
		System.out.println("Division = " + division.operation(12,4));
	}
}
