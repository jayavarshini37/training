/* 
Requirement
    To store and retrieve elements in array dequeue

Entity
    ArrayDequeue

Method Signature
    public static void main(String[] args)

Jobs to be done
    Create an array list 
    Add 7 values using addFirst(),addLast(),add methods in the dequeue
    Print peek values using peekFirst(),peekLast() methods in the dequeue
    Remove element using pollFirst(),pollLast(),removeFirst(),removeLast() methods in the dequeue
    Print the dequeue

Pseudo Code:
public class ArrayDequeueDemo {
	public static void main(String [] args) {
		Deque<String> color = new LinkedList<String>();
		color.addFirst(" ")
		color.add("");
        color.addLast(" ");
        System.out.println("Peek First Value: "+car.peekFirst()); //peek first value
		System.out.println("Peek Last Value: "+car.peekLast()); //peek last value
		color.pollFirst();  //poll first value
		color.pollLast();   //poll last value
		color.removeFirst();  //remove first value
		color.removeLast();   //remove last value
		System.out.println(color);
 */
package com.java.training.collection;

import java.util.LinkedList;
import java.util.Deque;

public class ArrayDequeue {
	public static void main(String [] args) {
		Deque<String> color = new LinkedList<String>();
		color.addFirst("Blue");  //add at first
		color.add("Green");
		color.add("Pink");
		color.add("Violet");
		color.add("Indigo");
		color.addLast("Red");   //add at last
		System.out.println("Peek First Value: "+ color.peekFirst()); //peek first value
		System.out.println("Peek Last Value: "+ color.peekLast()); //peek last value
		color.pollFirst();  //poll first value
		color.pollLast();   //poll last value
		color.removeFirst();  //remove first value
		color.removeLast();   //remove last value
		System.out.println(color);
	}

}