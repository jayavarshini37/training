/*
Requirement
    To add and remove elements in the stack

Entity
    AddRemoveStack

Method Signature
    public static void main(String [] args)

Jobs to be done
    1.Create a stack 
    2.Add the ements in the stack
    3.Pop the top element in the stack
    4.Print the stack

Pseudo Code:
public class AddRemoveStack {
    public static void main(String [] args) {
        Stack<String> fruits = new Stack<String>();  //stack using generic type
        fruits.push
 		fruits.pop()
 */
package com.java.training.collection;

import java.util.Stack;

import org.testng.annotations.Test;
@Test
public class AddRemoveStack {
    public static void main(String [] args) {
        Stack<String> colour = new Stack<String>();  //stack using generic type
        colour.push("Blue"); //adding a element to stack
        colour.push("Green");
        colour.push("Orange");
        colour.push("Yellow"); 
        colour.pop();  //removing a element to stack
        System.out.println(colour);
    }
}