/*
Requirement
    To convert the districts into Uppercase

Entity
    Uppercase

Method Signature
    public static void main(String[] args)

Jobs to be done
    1.Create a list named city
    2.Add 8 values in the list
    3.Convert the list into UPPERCASE
    4.Print the list

Pseudo Code:
public class Uppercase {
	public static void main(String[] args) {
	List<String> city = new ArrayList<String>();
	city.add(" "); 
	for(String i : city) {
	     System.out.println(i.toUpperCase() );
	    }
      
     */
package com.java.training.collection;

import java.util.ArrayList;
import java.util.List;

public class Uppercase {
	public static void main(String[] args) {
	List<String> city = new ArrayList<String>();
	city.add("Madurai"); 	//Add 8 values in the list
	city.add("Coimbatore");
	city.add("Theni");
	city.add("Chennai");
	city.add("Karur");
	city.add("Salem");
	city.add("Erode");
	city.add("Trichy ");
	for(String i : city) {
	     System.out.println(i.toUpperCase() );
	    }
    }
}