/*
Requirement
    To display the names starting with "A"

Entity
    StudentList

Method Signature
    public static void main(String[] args)

Jobs to be done
    1.Create a list name student
    2.Add 10 values in the list
    3. Display only names starting with 'A'

Pseudo Code:
public class StudentList {
	public static void main(String[] args) {
	List<String> student = new ArrayList<String>();
	student.add("");
	for(String i : student) {
	    if(i.startsWith("a")) System.out.println(i);
	    }
  
    */
package com.java.training.collection;

import java.util.ArrayList;
import java.util.List;


public class StudentList {
	public static void main(String[] args) {
	List<String> student = new ArrayList<String>();
	student.add("krishnan"); 	//Add 10 values in the list
	student.add("abishek");
	student.add("arun");
	student.add("vignesh");
	student.add("kiruthiga");
	student.add("murugan");
	student.add("adhithya");
	student.add("balaji");
	student.add("vicky");
	student.add("priya");
	for(String i : student) {
	    if(i.startsWith("a")) System.out.println(i);
	    }
    }
 }
