/*  
Requirement
    Code for sorting vector list in descending order.

Entity
    DesendingSort

Method Signature
    public static void main (String [] args)

Jobs to be done
    1.Create a vector 
    2.Add 6 values in the vector
    3.Sort the vector in descending order 
    4.Print the result

Pseudo Code:
public class SortVectorDesending {
	public static void main (String [] args) {
		Vector<String> color  = new Vector<String>();
		color .add(" ");
		System.out.println(color);
		Collections.sort(color , Collections.reverseOrder());		 
		System.out.println(color);
	}
}


 */
package com.java.training.collection;

import java.util.Collections;
import java.util.Vector;

public class DescendingSort {
	public static void main (String [] args) {
		Vector<String> color = new Vector<String>();
		color.add("Yellow");
		color.add("Green");
		color.add("Blue");
		color.add("Orange");
		color.add("Purple");
		color.add("Brown");
		System.out.println(color);
		Collections.sort(color , Collections.reverseOrder());		 
		System.out.println(color);
	}
}
