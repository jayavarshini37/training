/*
Requirements:
    What is Method Reference and its types.
    Write a program for each types with suitable comments.

Entities:
    Addition
    Reference

Interfaces:
    StaticMethod
    ParameterMethod
    InstanceMethod
    ConstructorReference
   
Method Signature:
    public static void display()
    public void printer()
    public int find(String string1, String string2)
    public void print(String string)
    Addition printAdd(int number1,int number2)
    Addition(int number1,int number2)(Constructor)
    public static void main(String[] args)

Jobs to be done:
    1. Declare the method public void printer() inside the interface StaticMethod
         and declare the method public int find(String string1,String string2) inside
         the interface Parametrized
    2. Declare the method public void print(String string) inside the interface
         InstanceMethod
    3. Declare the method Addition printAdd(int number1,int number2) inside the 
         interface Constructor Reference
    4. Refer the static method by creating "StaticMethod printStatement = Reference::display"
         and call the inteface method and print the result
    5. Find an first occurence by  indexOf() method 
         and call the method then print the result
    6. Call the method in interface Instance Method Reference and prin the result
    7. Find the addition of two numbers  in the interface constructor Reference
    
Pseudo code:
//create an interface
interface StaticMethod {
    public void printer();
//create an interface with two String parameters
interface ParameterMethod {
    public int find(String string1, String string2);
//create an interface with a String parameter
interface InstanceMethod {
    public void print(String string);
//create an interface declaring the single printAdd() method type of className with two integer parameters
interface ConstructorReference {  
    Addition printAdd(int number1,int number2)
class Addition {   
	Addition(int number1,int number2) {  
    //print the result  
    }
    
      public class MethodReference {
	       public static void display(){  
               //print static method reference
           public static void main(String[] args) {
    	       StaticMethod printStatement = MethodReference::display;  
    	       //print the output
    	       printStatement.printer();
               //Reference indexOf() method inside String class method
                 ParameterMethod finder = String::indexOf;
                 System.out.println("Parameter Method Reference");
                 System.out.println(finder.find("Parameter Method Reference","Reference"));
              //Reference println() method inside instance(out) of System class
                 InstanceMethod myPrinter = System.out::println;
                 System.out.println("Instance Method Reference");
                 myPrinter.print("Instance Method Reference");
              //Reference new keyword using Addition constructor.
                 ConstructorReference constructorReference = Addition::new;
                 System.out.println("Constructor Reference");
                 constructorReference.printAdd(2,6);
             
 /* Method Reference:
     Method reference is used to refer method of functional interface. It is compact and easy form of lambda expression.
       It is also called as special form of lambda expression.
   Types:
       1.Static Method Reference
       2.Parameter Method Reference
       3.Instance Method Reference
       4.Constructor Reference
       
*/
package com.java.training.lambda;
interface StaticMethod {
	public void printer(); 
}

interface ParameterMethod {
	public int find(String string1, String string2);
}

interface InstanceMethod {
	public void print(String string);
}

interface ConstructorReference {  

	Addition printAdd(int number1,int number2);  
    /*The printAdd() method of this interface matches the signature of one of the constructors 
in the Message class.*/
}

class Addition{   
    //Constructor of Addition class
	Addition(int number1,int number2) {  
        System.out.print(number1 + number2);  
    }  
}  

public class Reference {

	public static void display(){  
        System.out.println("Static Method Reference");  
    }  
	
    public static void main(String[] args) {  
        // Referring static method  
    	StaticMethod printStatement = Reference::display;  
        // Calling interface method
    	System.out.println("/t Static Method Reference");
    	printStatement.printer();
        
    	
        ParameterMethod finder = String::indexOf;
        System.out.println("\n Parameter Method Reference");
        System.out.println(finder.find("Parameter Method Reference","Reference"));
        
        
        InstanceMethod myPrinter = System.out::println;
        System.out.println("\n Instance Method Reference");
        myPrinter.print("Instance Method Reference");
        
        
        ConstructorReference constructorReference = Addition::new;
        System.out.println("\n Constructor Reference");
        constructorReference.printAdd(2,6);
    } 
}