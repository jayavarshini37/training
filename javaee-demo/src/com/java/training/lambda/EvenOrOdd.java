/*
Requirement:
    Convert the following anonymous class into lambda expression.

interface CheckNumber {
    public boolean isEven(int value);
}

public class EvenOrOdd {
    public static void main(String[] args) {
        CheckNumber number = new CheckNumber() {
            public boolean isEven(int value) {
                if (value % 2 == 0) {
                    return true;
                } else return false;
            }
        };
        System.out.println(number.isEven(11));
    }
}

Entities:
    EvenOrOdd
    TestNumber (Interface)

Method Declaration:
    public static void main(String[] args)
    public boolean isEven(int value)
   
Jobs to be done:
    1. Declare the method isEven() inside the interface CheckNumber()
    2. Using LambdaEpression CheckNumber number = (value) 
        2.1 check whether the value is divisible by 2
            2.1.i) Return true, if it divisible by 0
            2.1.ii) Return false, if it is not divisible by 0
    3. Call the method isEven() and print the result

Pseudo code:
interface CheckNumber {
    public boolean isEven(int value);
}     
public class EvenOrOdd {
    public static void main(String[] args) {
        CheckNumber number = (value) -> { 
            if (value % 2 == 0) {
                return true;
            } else return false;
        };
        //print the result
            System.out.println(number.isEven(10));
*/

package com.java.training.lambda;

interface CheckNumber {
    public boolean isEven(int value);
}

public class EvenOrOdd {
    public static void main(String[] args) {
        
        CheckNumber number = (value) -> { // Convert into Java Lambda Expression 
                if (value % 2 == 0) {
                    return true;
                } else return false;
            };
        System.out.println(number.isEven(10));
    }
}