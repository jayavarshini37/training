/*
1.Requirement:
    To find the wrong in the program and fix it with type reference
    
interface BiFunction{
    int print(int number1, int number2);
}

public class TypeInferenceExercise {
    public static void main(String[] args) {

        BiFunction function = (int number1, int number2) ->  { 
        return number1 + number2;
        };
        
        int print = function.print(int 23,int 32);    
        
        System.out.println(print);
    }
}

Entities:
    TypeInferenceExercise
    BiFunction (Interface)

Method signature:
    int print(int number1, int number2);
    public static void main(String[] args)

Jobs to be done:
    1. Declare the method int print(int number1, int number2) inside the 
        interface BiFunction
    2. Perform addition of two numbers by lambda expression
    3. Call the method and print the result 

Pseudo code:
interface BiFunction{
    int print(int number1, int number2);
}

public class TypeReference {
    public static void main(String[] args) {

        BiFunction function = (number1, number2) -> number1 + number2; // Don't want to specify type and single brackets { } for single statement
        int print = function.print(11,12); // passing values
        System.out.println(print);
    }
} 
  */
package com.java.training.lambda;

interface BiFunction{
    int print(int number1, int number2);
}

public class TypeReference {
    public static void main(String[] args) {

        BiFunction function = (number1, number2) -> number1 + number2; // Don't want to specify type and single brackets { } for single statement
        int print = function.print(11,12); // Don't want to specify type while passing value
        System.out.println(print);
    }
}