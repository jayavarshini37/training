/* 
Requirement:
    To code a functional program that can return the sum of elements of varArgs, passed into the  method of functional interface.

Entities:
    VarArgsInterface
    MultipleValues (Interface)

Method Signature:
    int sum(int... numbers);
    public static void main(String[] args)

Jobs to be done
    1.Create a class as VarArgsInterface with interface as MultipleValues.
    2.Inside the declaring the single method with multiple integer parameters using varargs.
    3.In the class main creating interface object and assigning with passing integer values returning the addition of two values.
    4.Print statement invoking the interface single method with multiple integer value and finally return all added value using assigned
        interface object lambda expression.
        
Pseudo code:
 interface MultipleValues {   //creating an interface
     int sum(int... numbers);
}	     
public class VarargsInterface {
    public static void main(String[] args) {
	      MultipleValues multipleValues = (int... numbers) -> {
	      int sum = 0;
		  for (int i : numbers)  
		      sum += i;  
		  return sum; 
			};
		  System.out.println(multipleValues.sum(1,2,3,4,5));
		  //print the result invoking interface single method with multiple integer value

*/
package com.java.training.lambda;

interface MultipleValues {
	int sum(int... numbers);
}
public class VarInterface {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MultipleValues multipleValues = (int... numbers) -> {
		int sum = 0;
		for (int i : numbers)  
		    sum += i;  
		return sum; 
		};
		System.out.println(multipleValues.sum(1,2,3,4,5));
	}

}