/*  
Requirement:
    Program to print the volume of a Rectangle using lambda expression.

Entities:
    Rectangle
    PrismValues (Interface)

Method signature:
    int Values(int base,int length,int height);
    public static void main(String[] args)

Jobs to be done:
    1. Declare the method int Values() inside the interface PrismValues
    2. Calculate the volume of the rectangle using lambda expression
    3. Call the method values 
        3.1 Print the result 
        
Pseudo code:
interface PrismValues {   //creating an interface
    int values(int base,int length,int height);
}	     
public class Rectangle {
    public static void main(String[] args) {    
	    PrismValues prismValues = (base,length,height) -> base*length*height;  //calling the interface with an object and perform the operation
	    System.out.println(prismValues.Values(2,4,6));
	    //print the statement invoking the interface with multiple integer value
*/

package com.java.training.lambda;

interface PrismValues {
	int Values(int base,int length,int height);
}
public class Rectangle {

	public static void main(String[] args) {
		PrismValues prismValues = (base,length,height) -> base * length * height;
		System.out.println(prismValues.Values(2,4,6));

	}

}