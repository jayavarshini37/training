/*
1.Requirement:
    Program to print difference of two numbers using lambda expression and the single method interface

2.Entities:
    DifferenceNumber
    Different (Interface)

3.Method Signature:
    int numbers(int number1,int number2);
    public static void main(String[] args)

4.Jobs to be done
    1. Declare the method int numbers(int number1,int number2) inside the interface Difference
    2. Calculate the difference of two numbers using lambda expression 
        2.1 call the method int numbers(int number1,int number2) and print the result  

5.Pseudo code:
interface Difference {
    int numbers(int number1,int number2);
}
public class DifferenceNumber {
    public static void main(String[] args) {
	    Different different = (number1,number2) -> number1 - number2;
	        //Print the difference of the numbers
*/ 

package com.java.training.lambda;

interface Difference {
	int numbers(int number1,int number2);
}

public class DifferenceNumber {
	public static void main(String[] args) {
		Difference different = (number1,number2) -> number1 - number2;
		System.out.println(different.numbers(100,50));
	}
}