/*
1.Requirement:
    To write a Lambda expression Program with a single method interface to concatenate two strings

2.Entities:
    Concatenate
    SingleMethodInterface (Interface)

3.Method signature:
    String string(String string1, String string2);
    public static void main(String[] args)

4.Jobs to be done:
    1.Inside the class declare the single method with two string parameters
    2.In the main class create interface object 
        2.1 Assign the lambda expression to return two strings concatenate 
    3.Print the statement invoking the interface single method with string values 
        3.1 Return the value using assigned interface object lambda expression 

5.Pseudo code:
interface SingleMethodInterface {
	String string(String string1, String string2);
}
public class Concatenate {

	public static void main(String[] args) {
	   
        SingleMethodInterface singleInterface = (string1,string2) -> string1.concat(string2);
	    
	    //Print the result
	      
*/
package com.java.training.lambda;

interface SingleMethodInterface {
	String string(String string1, String string2);
}

public class Concatenate {

	public static void main(String[] args) {
		//Assigning the lambda expression to return two strings concatenate
		SingleMethodInterface singleInterface = (string1,string2) -> string1.concat(string2);
		//invoking the interface single method with string values 
		System.out.println(singleInterface.string("Hello ", "Welcome to this show"));
	}

}