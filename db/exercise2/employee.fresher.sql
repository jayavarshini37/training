10. Prepare a query to find out fresher(no department allocated employee) in the employee, where no matching records in the department table.


ALTER TABLE `sample`.`employee` 
DROP FOREIGN KEY `dept_id`;
ALTER TABLE `sample`.`employee` 
CHANGE COLUMN `department_id` `department_id` VARCHAR(7) NULL ;
ALTER TABLE `sample`.`employee` 
ADD CONSTRAINT `dept_id`
  FOREIGN KEY (`department_id`)
  REFERENCES `sample`.`department` (`department_id`);

INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `area`) VALUES ('gh1', 'harini', '2000-06-10', '2019-03-29', '380000', 'coimbatore');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `sur_name`, `dob`, `date_ of_ joining`, `annual_salary`, `area`) VALUES ('gh2', 'jannet', 'mariya', '2000-12-27', '2018-08-17', '420000', 'coimbatore');

SELECT employee_id
    ,first_name
	,sur_name
	,dob
	,area
	from employee 
	where department_id is NULL; 