

2. Write a query to insert at least 5 employees for each department


UPDATE `sample`.`department` SET `department_name` = 'ITDesk' WHERE (`department_id` = '101');
UPDATE `sample`.`department` SET `department_id` = '106', `department_name` = 'Facility' WHERE (`department_id` = '106');
UPDATE `sample`.`department` SET `department_id` = '105', `department_name` = 'Recruitment' WHERE (`department_id` = '105');
UPDATE `sample`.`department` SET `department_id` = '104', `department_name` = 'HR' WHERE (`department_id` = '104');
UPDATE `sample`.`department` SET `department_id` = '103', `department_name` = 'Engineering' WHERE (`department_id` = '103');
UPDATE `sample`.`department` SET `department_id` = '102', `department_name` = 'Finance' WHERE (`department_id` = '102');


INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('tcs1', 'jaya', '2000-09-07', '2019-08-04', '250000', '101');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('tcs2', 'abi', '2000-01-06', '2019-07-02', '250000', '101');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('cts1', 'jevetha', '1999-06-05', '2018-05-03', '300000', '101');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('cts3', 'swathi', '1999-09-05', '2018-05-23', '280000', '101');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `sur_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('ibc1', 'sneha', 'natrajan', '1999-01-02', '2018-04-13', '280000', '102');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('cts2', 'anbu', '1999-04-25', '2018-06-09', '300000', '101');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `sur_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('ibc2', 'joe', 'immanuel', '1997-08-23', '2017-04-09', '280000', '102');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `sur_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('ibc3', 'annie', 'selina', '1998-08-25', '2018-10-27', '280000', '102');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `sur_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('ibc4', 'merlin', 'rachel', '1999-05-20', '2018-11-06', '300000', '102');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `sur_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('ibc5', 'samuel', 'paul', '1997-09-28', '2018-06-11', '300000', '102');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `sur_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('psg1', 'jerome', 'festus', '2000-05-01', '2019-08-23', '320000', '103');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `sur_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('psg2', 'jemy', 'sharon', '2000-06-01', '2019-09-18', '320000', '103');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('psg3', 'priya', '1999-08-26', '2018-02-09', '320000', '103');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('psg4', 'varshini', '2000-07-03', '2018-03-21', '320000', '103');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('psg5', 'suganthi', '2000-10-14', '2019-02-01', '320000', '103');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('ab1', 'mega', '2000-07-06', '2018-03-09', '400000', '104');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('ab2', 'nithisree', '2000-04-15', '2018-09-12', '400000', '104');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('ab3', 'mathu', '2000-01-01', '2019-12-26', '360000', '104');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('ab4', 'ranjani', '2000-09-18', '2018-06-16', '380000', '104');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `sur_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('ab5', 'dhana', '', '1999-06-24', '2019-11-19', '380000', '104');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `sur_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('pq1', 'janani', 'yogamurali', '2000-01-10', '2019-10-16', '420000', '105');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `sur_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('pq2', 'vignesh', 'manoharan', '2000-04-28', '2018-12-12', '420000', '105');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('pq3', 'kathir', '1998-09-10', '2017-07-19', '400000', '105');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('pq4', 'raswanth', '1999-08-26', '2019-11-21', '420000', '105');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('pq5', 'praveen', '1999-09-29', '2019-01-18', '400000', '105');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('cm1', 'kaviya', '1999-04-09', '2018-03-03', '800000', '106');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('cm2', 'keerthana', '2000-05-15', '2018-01-29', '800000', '106');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('cm3', 'lakshmi', '1999-11-28', '2019-06-26', '750000', '106');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `sur_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('cm4', 'bakhita', 'jessila', '2000-11-24', '2019-01-27', '780000', '106');
INSERT INTO `sample`.`employee` (`employee_id`, `first_name`, `dob`, `date_ of_ joining`, `annual_salary`, `department_id`) VALUES ('cm5', 'hephsiba', '2000-11-01', '2019-12-26', '820000', '106');
