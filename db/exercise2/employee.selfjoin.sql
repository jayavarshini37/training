7. Prepare an example for self-join

SELECT e1.first_name
    ,e1.area
FROM employee e1
    ,employee e2
WHERE e1.area = e2.area
AND e2.first_name="jaya"


Steps:
 Two separate copies of the same table sample table is created for aliases e1 and e2
 e1=employee1
 e2=employee2
 if employee1 area= employee2 area
 the value of the first employee name can be changed into second employee name 
   
