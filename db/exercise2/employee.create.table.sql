
1. Create tables to hold employee(emp_id, first_name, surname, dob, date_of_joining, annual_salary), possible depts: ITDesk, Finance, Engineering, HR, Recruitment, Facility

    employee number must be the primary key in employee table
    department number must be the primary key in department table
    department number is the foreign key in the employee table 
3. Add the Primary key to employee number column


CREATE TABLE `sample`.`employee` (
  `employee_id` VARCHAR(12) NOT NULL,
  `first_name` VARCHAR(25) NOT NULL,
  `sur_name` VARCHAR(10) NULL,
  `dob` DATE NOT NULL,
  `date_ of_ joining` DATE NOT NULL,
  `annual_salary` INT(10) NOT NULL,
  `dept_id` VARCHAR(7) NOT NULL);



CREATE TABLE `sample`.`department` (
  `department_id` VARCHAR(12) NOT NULL,
  `department_name` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`department_id`));
  
  ALTER TABLE `sample`.`employee` 
ADD INDEX `department_id_idx` (`dept_id` ASC) VISIBLE;
;
ALTER TABLE `sample`.`employee` 
ADD CONSTRAINT `dept_id`
  FOREIGN KEY (`dept_id`)
  REFERENCES `sample`.`department` (`department_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `sample`.`employee` 
DROP FOREIGN KEY `dept_id`;
ALTER TABLE `sample`.`employee` 
CHANGE COLUMN `dept_id` `department_id` VARCHAR(7) NOT NULL ;
ALTER TABLE `sample`.`employee` 
ADD CONSTRAINT `dept_id`
  FOREIGN KEY (`department_id`)
  REFERENCES `sample`.`department` (`department_id`);

ALTER TABLE `sample`.`employee` 
DROP FOREIGN KEY `dept_id`;
ALTER TABLE `sample`.`employee` 
ADD CONSTRAINT `department_id`
  FOREIGN KEY (`dept_id`)
  REFERENCES `sample`.`department` (`department_id`);

ALTER TABLE `sample`.`employee` 
ADD PRIMARY KEY (`employee_id`);
;
