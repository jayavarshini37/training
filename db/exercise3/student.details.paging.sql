SELECT student.roll_number
       ,student.name
       ,college.name
       ,semester_result.grade
       ,semester_result.credits
       ,semester_result.semester
       ,semester_result.gpa
FROM student
     ,semester_result
     ,college
WHERE student.id=semester_result.stud_id
   AND college.id=student.college_id
ORDER BY college.name,semester_result.semester
LIMIT 20
OFFSET 0;


SELECT s.name
        , s.roll_number
        , s.gender
        , c.name
        , c.city
        , sr.grade
        , sr.credits
        , sr.semester
    FROM student s
    LEFT JOIN  college c ON s.college_id=c.id
    LEFT JOIN semester_result sr ON sr.stud_id=s.id
    ORDER BY c.name AND semester
    LIMIT 1,3