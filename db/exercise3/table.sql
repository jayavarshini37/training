CREATE database education;
USE education;
CREATE table university(univ_code char(4)
                        ,university_name varchar(100) not null
                        ,primary key(univ_code));

CREATE table college(id int
                     ,code char(4) not null
                     ,name varchar(100)not null
                     ,univ_code char(4)
                     ,city varchar(50) not null
                     ,state varchar(50) not null
                     ,year_opened year(4) not null
                     ,primary key(id)
                     ,foreign key (univ_code) references university(univ_code));
        
CREATE table department(dept_code char(4)
                        ,dept_name varchar(50) not null
                        ,univ_code char(4)
                        ,primary key(dept_code)
                        ,foreign key (univ_code) references university(univ_code));
                        
CREATE table college_department(cdept_id int
                                ,udept_code char(4)
                                ,college_id int
                                ,primary key(cdept_id)
                                ,foreign key (college_id) references college(id)
                                ,foreign key(udept_code)references department(dept_code));

CREATE table syllabus(id int  
                       ,cdept_id int
                       ,syllabus_code char not null
                       ,syllabus_name varchar(100)not null
                       ,primary key(id)
                       ,foreign key(cdept_id)references college_department(cdept_id));
                       
CREATE table designation(id int
                         ,name varchar(30)not null
                         ,ranks char(1)not null
                         ,primary key(id));
                         
CREATE table employee(id int,
                       name varchar(100) not null
                       ,dob date not null
                       ,email varchar(50)not null
                       ,phone bigint not null
                       ,college_id int
                       ,cdept_id int
                       ,desig_id int
                       ,foreign key(college_id)references college(id)
                       ,foreign key(cdept_id)references college_department(cdept_id)
                       ,foreign key(desig_id)references designation(id));
                       
ALTER table employee
ADD primary key(id);

CREATE table professsor_syllabus(emp_id int
                                ,syllabus_id int
                                ,semester tinyint not null
                                ,foreign key(emp_id)references employee(id)
                                ,foreign key (syllabus_id)references syllabus(id));

CREATE table student(id int
                      ,roll_number char(8) not null
                      ,name varchar(100) not null
                      ,dob date not null
                      ,gender char(1)not null
                      ,email varchar(50) not null
                      ,phone bigint not null
                      ,address varchar(200) not null
                      ,academic_year year not null
                      ,cdept_id int
                      ,college_id int
                      ,primary key(id)
                      ,foreign key(cdept_id)references college_department(cdept_id)
                      ,foreign key(college_id)references college(id));
                      
CREATE table semester_fee(cdept_id int
                        ,stud_id int
                        ,semester tinyint not null
                        ,amount double(18,2)
                        ,paid_year year
                        ,paid_status varchar(10)
                        ,foreign key(cdept_id)references college_department(cdept_id)
                        ,foreign key(stud_id)references student(id));
                        
CREATE table semester_result(stud_id int
                             ,syllabus_id int
                             ,semester tinyint not null
                             ,grade varchar(2) not null
                             ,credits float 
                             ,result_date date not null
                             ,foreign key(stud_id)references student(id)
                             ,foreign key(syllabus_id)references syllabus(id));