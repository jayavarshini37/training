UPDATE `education`.`semester_fee` SET `bill_number` = '401' WHERE (`s_no` = '1') and (`bill_number` = '0');
UPDATE `education`.`semester_fee` SET `bill_number` = '402' WHERE (`s_no` = '2') and (`bill_number` = '0');
UPDATE `education`.`semester_fee` SET `bill_number` = '403' WHERE (`s_no` = '3') and (`bill_number` = '0');
UPDATE `education`.`semester_fee` SET `bill_number` = '404' WHERE (`s_no` = '4') and (`bill_number` = '0');
UPDATE `education`.`semester_fee` SET `bill_number` = '405' WHERE (`s_no` = '5') and (`bill_number` = '0');
UPDATE `education`.`semester_fee` SET `bill_number` = '406' WHERE (`s_no` = '6') and (`bill_number` = '0');

ALTER TABLE `education`.`semester_fee` 
DROP PRIMARY KEY,
ADD PRIMARY KEY (`bill_number`);
;

ALTER TABLE `education`.`semester_fee` 
CHANGE COLUMN `amount` `amount` DOUBLE(18,2) NULL DEFAULT 45000 ,
CHANGE COLUMN `paid_status` `paid_status` VARCHAR(10) NULL DEFAULT 'unpaid' ;

