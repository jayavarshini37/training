SELECT designation.name AS designation_name
  ,ranks
  ,university.univ_code
  ,college.name AS college_name
  ,department.dept_name
  ,university.university_name
  ,college.city
  ,college.state
  ,college.year_opened
 FROM university 
  ,college 
  ,department 
  ,designation 
  ,college_department 
  ,employee  
 WHERE college.univ_code = university.univ_code 
 AND university.univ_code = department.univ_code 
 AND college_department.college_id = college.id 
 AND college_department.udept_code = department.dept_code
 AND employee.college_id = college.id 
 AND employee.cdept_id = college_department.cdept_id 
 AND employee.desig_id = designation.id 
 ORDER BY ranks;