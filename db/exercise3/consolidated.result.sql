a)

SELECT university.university_name
      ,college.name
      ,semester_fee.semester
      ,SUM(amount) AS collected_fees 
      ,semester_fee.paid_year
FROM semester_fee
    ,university
    ,student
    ,college
WHERE semester_fee.stud_id = student.id
 AND student.college_id = college.id
 AND college.univ_code = university.univ_code
 AND university_name = 'Anna'
 AND paid_year = '2014'
 AND semester = '4'
 AND paid_status = 'unpaid';

b)

SELECT university.university_name
      ,SUM(amount) AS collected_fees 
      ,semester_fee.paid_year
FROM semester_fee
    ,university
    ,student
    ,college
WHERE semester_fee.stud_id = student.id
 AND student.college_id = college.id
 AND college.univ_code = university.univ_code
 AND university.university_name = 'Anna'
 AND paid_status = 'paid'
 AND paid_year = '2015'