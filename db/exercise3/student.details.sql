SELECT student.roll_number
	  ,student.name
      ,student.gender
      ,student.dob
      ,student.email
      ,student.phone
      ,student.address
      ,college.name
      ,department.dept_name
      ,employee.name

  FROM department
      ,student
	  ,employee
	  ,college_department
	  ,college
	  ,university
 WHERE university.univ_code = college.univ_code
   AND student.college_id = college.id 
   AND employee.college_id = college.id
   AND employee.desig_id = 201
   AND student.cdept_id = college_department.cdept_id
   AND college_department.udept_code = department.dept_code
   AND university.university_name = 'Anna'
   AND college.city = 'coimbatore'
   LIMIT 0,20;