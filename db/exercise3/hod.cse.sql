SELECT  college.code
       ,college.name AS college_name
       ,university.university_name 
       ,college.city
       ,college.state
       ,college.year_opened
       ,department.dept_name AS department_name
       ,employee.name AS hod_name
FROM college
    ,university
    ,department
    ,employee
    ,designation
	,college_department
WHERE college.univ_code = university.univ_code 
AND  department.univ_code = college.univ_code
AND  employee.college_id = college.id
AND  employee.cdept_id = college_department.cdept_id
AND  employee.desig_id = designation.id
AND  college_department.udept_code = department.dept_code = "DD"
AND  college_department.college_id = college.id
AND designation.name = "HOD"
AND department.dept_name = "CSE" OR "IT"
