
ALTER TABLE `education`.`semester_result` 
ADD COLUMN `gpa` FLOAT NOT NULL AFTER `result_date`;

UPDATE `education`.`semester_result` SET `gpa` = '7' WHERE (`credits` = '2.5');
UPDATE `education`.`semester_result` SET `gpa` = '7.5' WHERE (`credits` = '3.2');
UPDATE `education`.`semester_result` SET `gpa` = '8' WHERE (`credits` = '4');
UPDATE `education`.`semester_result` SET `gpa` = '8.7' WHERE (`credits` = '4.3');
UPDATE `education`.`semester_result` SET `gpa` = '9' WHERE (`credits` = '4.5');
UPDATE `education`.`semester_result` SET `gpa` = '10' WHERE (`credits` = '5');

a)

SELECT student.id
	  ,student.roll_number
      ,student.name
      ,student.gender
      ,college.code
      ,college.name
      ,semester_result.grade
      ,semester_result.credits
      ,semester_result.gpa
FROM university
      ,college
      ,student
      ,semester_result
WHERE university.univ_code = college.univ_code
 AND student.college_id = college.id
 AND semester_result.stud_id = student.id
 AND semester_result.gpa>8;

b)
   
SELECT student.id
	  ,student.roll_number
      ,student.name
      ,student.gender
      ,college.code
      ,college.name
      ,semester_result.grade
      ,semester_result.credits
      ,semester_result.gpa
FROM university
      ,college
      ,student
      ,semester_result
WHERE university.univ_code = college.univ_code
 AND student.college_id = college.id
 AND semester_result.stud_id = student.id
 AND semester_result.gpa>5
 ORDER BY gpa;