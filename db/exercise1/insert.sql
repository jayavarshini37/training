5.Insert Records into Table


INSERT INTO `hotel`.`hotel` (`hotel_name`, `dish`, `table_number`, `order_number`, `amount`) VALUES ('Ariyas', 'naan', '01', '001', '80');
INSERT INTO `hotel`.`hotel` (`hotel_name`, `dish`, `table_number`, `order_number`, `amount`) VALUES ('Saravana bhavan', 'mushroom briyani', '02', '003', '180');
INSERT INTO `hotel`.`hotel` (`hotel_name`, `dish`, `table_number`, `order_number`, `amount`) VALUES ('Haribhavan', 'meat balls', '03', '004', '110');
INSERT INTO `hotel`.`hotel` (`hotel_name`, `dish`, `table_number`, `order_number`, `amount`) VALUES ('Annapoorna', 'ghee roast', '04', '007', '50');
INSERT INTO `hotel`.`hotel` (`hotel_name`, `dish`, `table_number`, `order_number`, `amount`) VALUES ('Taj', 'fish curry', '05', '009', '120');


UPDATE `hotel`.`hotel` SET `dessert` = 'black forest' WHERE (`hotel_name` = 'Annapoorna');
UPDATE `hotel`.`hotel` SET `dessert` = 'honey cake' WHERE (`hotel_name` = 'Ariyas');
UPDATE `hotel`.`hotel` SET `dessert` = 'malva pudding' WHERE (`hotel_name` = 'Saravana bhavan');
UPDATE `hotel`.`hotel` SET `dessert` = 'black current icecream' WHERE (`hotel_name` = 'Taj');
