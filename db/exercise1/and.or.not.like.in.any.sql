9.USE Where to filter records using AND,OR,NOT,LIKE,IN,ANY, wildcards ( % _ )


SELECT dish FROM hotel WHERE order_number="7" AND amount="50";
SELECT dish FROM hotel WHERE order_number="2" OR amount="50";
SELECT dish FROM hotel WHERE NOT order_number="1";
SELECT dessert FROM hotel WHERE dish LIKE "%h%";
SELECT dessert FROM hotel WHERE dish IN ("ghee roast");

UPDATE `hotel`.`hotel` SET `customer_name` = 'menaga' WHERE (`hotel_name` = 'Annapoorna');
UPDATE `hotel`.`hotel` SET `customer_name` = 'jaya' WHERE (`hotel_name` = 'Ariyas');
UPDATE `hotel`.`hotel` SET `customer_name` = 'jannet' WHERE (`hotel_name` = 'Saravana bhavan');
UPDATE `hotel`.`hotel` SET `customer_name` = 'mega' WHERE (`hotel_name` = 'Taj');

SELECT dish FROM hotel WHERE dish=any(SELECT dessert from hotel WHERE dessert="black forest");

