11.Read Records by Sub-queries by using two tables FOREIGN KEY relationships

INSERT INTO `hotel`.`server` (`server_id`, `server_name`) VALUES ('1', 'chandran');
INSERT INTO `hotel`.`server` (`server_id`, `server_name`) VALUES ('2', 'lincoln');
INSERT INTO `hotel`.`server` (`server_id`, `server_name`) VALUES ('3', 'bose');
INSERT INTO `hotel`.`server` (`server_id`, `server_name`) VALUES ('4', 'thilak');

UPDATE `hotel`.`hotel` SET `server_id` = '1' WHERE (`hotel_name` = 'Annapoorna');
UPDATE `hotel`.`hotel` SET `server_id` = '2' WHERE (`hotel_name` = 'Ariyas');
UPDATE `hotel`.`hotel` SET `server_id` = '3' WHERE (`hotel_name` = 'Saravana bhavan');
UPDATE `hotel`.`hotel` SET `server_id` = '4' WHERE (`hotel_name` = 'Taj');

select hotel.hotel_name
    ,server.server_name 
	from hotel join server on hotel.server_id=server.server_id;