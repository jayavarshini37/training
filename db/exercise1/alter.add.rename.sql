2.Alter Table with Add new Column and Modify\Rename\Drop column


ALTER TABLE `hotel`.`hotel` 
CHANGE COLUMN `food_name` `dish` VARCHAR(45) NOT NULL ;

ALTER TABLE `hotel`.`hotel` 
ADD COLUMN `dessert` VARCHAR(25) NOT NULL AFTER `amount`;