1.Create Database\table and Rename\Drop Table

CREATE SCHEMA `hotel` ;

CREATE TABLE `hotel`.`hotel` (
  `hotel_name` VARCHAR(25) NOT NULL,
  `food_name` VARCHAR(25) NOT NULL,
  `table_number` INT(5) NOT NULL,
  `order_number` INT(4) NOT NULL,
  `amount` INT(7) NOT NULL,
  PRIMARY KEY (`hotel_name`));
  
  
  CREATE TABLE `hotel`.`new_table` (
  `customer_id` INT(10) NOT NULL,
  `customer_name` VARCHAR(35) NOT NULL,
  PRIMARY KEY (`customer_id`));

INSERT INTO `hotel`.`new_table` (`customer_id`, `customer_name`) VALUES ('1001', 'mega');
INSERT INTO `hotel`.`new_table` (`customer_id`, `customer_name`) VALUES ('1002', 'jaya');
INSERT INTO `hotel`.`new_table` (`customer_id`, `customer_name`) VALUES ('1003', 'nithi');
INSERT INTO `hotel`.`new_table` (`customer_id`, `customer_name`) VALUES ('1004', 'menaga');
INSERT INTO `hotel`.`new_table` (`customer_id`, `customer_name`) VALUES ('1005', 'mathu');


DROP TABLE `hotel`.`new_table`;