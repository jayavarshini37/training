12.Read Records by Join using tables FOREIGN KEY relationships (CROSS, INNER, LEFT, RIGHT)


ALTER TABLE `hotel`.`hotel` 
CHANGE COLUMN `hotel_name` `name` VARCHAR(25) NOT NULL ;

select hotel.name
    ,server.server_name 
	from hotel inner join server on hotel.server_id=server.server_id; 
select hotel.name
    ,server.server_name 
	from hotel right join server on hotel.server_id=server.server_id;
select hotel.dish
    ,server.server_name 
	from hotel left join server on hotel.server_id=server.server_id;
select*from hotel 
    cross join server;
  