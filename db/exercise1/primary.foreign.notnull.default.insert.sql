
3.Use Constraints NOT NULL, DEFAULT, CHECK, PRIMARY KEY, FOREIGN KEY, KEY, INDEX


  ALTER TABLE `hotel`.`hotel` 
CHANGE COLUMN `order_number` `order_number` INT NULL ;

ALTER TABLE `hotel`.`hotel` 
ADD COLUMN `server_id` INT NULL AFTER `customer_name`;

CREATE TABLE `hotel`.`server` (
  `server_id` INT NOT NULL,
  `server_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`server_id`));
  
  ALTER TABLE `hotel`.`hotel` 
ADD INDEX `server_id_idx` (`server_id` ASC) VISIBLE;
;
ALTER TABLE `hotel`.`hotel` 
ADD CONSTRAINT `server_id`
  FOREIGN KEY (`server_id`)
  REFERENCES `hotel`.`server` (`server_id`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
  
  ALTER TABLE hotel ADD CHECK(amount > 0);
  
  ALTER TABLE `hotel`.`hotel` 
ADD UNIQUE INDEX `server_id_UNIQUE` (`server_id` ASC) VISIBLE;
;
CREATE INDEX idx_amount
    ON hotel (amount);


