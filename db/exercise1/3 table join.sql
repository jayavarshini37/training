
13.At-least use 3 tables for Join
Union 2 Different table with same Column name (use Distinct,ALL)



ALTER TABLE `hotel`.`server` 
CHANGE COLUMN `server_id` `id` INT NOT NULL ,
CHANGE COLUMN `server_name` `name` VARCHAR(45) NOT NULL ;

CREATE TABLE `hotel`.`customer` (
  `id` INT NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `hotel`.`customer` (`id`, `name`) VALUES ('5', 'nithisree');
INSERT INTO `hotel`.`customer` (`id`, `name`) VALUES ('6', 'mathu');
INSERT INTO `hotel`.`customer` (`id`, `name`) VALUES ('7', 'varshini');
INSERT INTO `hotel`.`customer` (`id`, `name`) VALUES ('8', 'kaviya');

select id,name from server union select id,name from customer;


ALTER TABLE `hotel`.`hotel` 
DROP FOREIGN KEY `server_id`;
ALTER TABLE `hotel`.`hotel` 
CHANGE COLUMN `server_id` `id` INT NULL DEFAULT NULL ;
ALTER TABLE `hotel`.`hotel` 
ADD CONSTRAINT `server_id`
  FOREIGN KEY (`id`)
  REFERENCES `hotel`.`server` (`id`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;

select hotel.dish
       ,server.id
	   ,customer.name
	   ,customer.id
from hotel 
inner join server on server.id=hotel.id
 inner join customer on hotel.id=customer.id order by server.id asc;