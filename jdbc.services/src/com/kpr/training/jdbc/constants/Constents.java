package com.kpr.training.jdbc.constants;

public class Constents {
	
	public static final String createAddressQuery =
			"INSERT INTO `jdbc`.address (street, city, postal_code) VALUES (?, ?, ?)";

	public static final String readAddressQuery =
			"SELECT address.id, address.street, address.city, address.postal_code FROM  `jdbc`.address WHERE `id` = ?";

	public static final String readAllAddressQuery =
			"SELECT address.id, address.street, address.city, address.postal_code FROM  `jdbc`.address";

	public static final String updateAddressQuery =
			"UPDATE `jdbc`.address SET street = ?, city = ?, postal_code = ? WHERE id = ?";

	public static final String deleteAddressQuery = "DELETE FROM  `jdbc`.address WHERE id=?";

	public static final String createPersonQuery =
			"INSERT INTO `jdbc`.person (name, email, birth_date, address_id) VALUES (?, ?, ?, ?)";

	public static final String readPersonQuery =
			"SELECT person.id, person.name, person.email, person.address_id, person.birth_date, person.created_date FROM  `jdbc`.person WHERE `id` = ?";

	public static final String readAllPersonQuery =
			"SELECT person.id, person.name, person.email, person.address_id, person.birth_date, person.created_date FROM  `jdbc`.person";

	public static final String updatePersonQuery =
			"UPDATE `jdbc`.person LEFT JOIN `jdbc`.address ON person.address_id = address.id SET address.street = ?, address.city = ?, address.postal_code = ?, person.name = ?, person.email = ?, person.birth_date = ? WHERE person.id = ?";

	public static final String deletePersonQuery =
			"DELETE `jdbc`.address, `jdbc`.person FROM `jdbc`.person INNER JOIN `jdbc`.address ON person.address_id = address.id WHERE person.id = ?";
	
	public static final String emailUnique = "SELECT person.id FROM `jdbc`.person WHERE person.email = ?";
}