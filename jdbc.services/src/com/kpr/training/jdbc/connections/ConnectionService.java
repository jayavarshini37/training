package com.kpr.training.jdbc.connections;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import com.kpr.training.jdbc.constants.Constants;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;

@SuppressWarnings("unused")
public class ConnectionService extends ThreadPoolExecutor{

    private static ThreadLocal<Connection> local = new ThreadLocal<>();
    
    private void init() {

        Properties properties = new Properties();

        try {
            InputStream resourceAsStream =
                    ConnectionService.class.getClassLoader().getResourceAsStream("db.properties");
            if (resourceAsStream != null) {
                properties.load(resourceAsStream);
            }
            
            Connection con = DriverManager.getConnection(properties.getProperty(Constants.URL),
                    properties.getProperty(Constants.USER_NAME), properties.getProperty(Constants.PASSWORD));
            con.setAutoCommit(false);
            
            local.set(con);

        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILS, e);
        } 
    }
    
    public static Connection get() {
        return local.get();
    }
    
    public static void release() {
        
        try {
            local.get().close();
            local.remove();
        } catch (Exception e) {
            throw new AppException(ErrorCode.CONNECTION_FAILS_TO_CLOSE, e);
        }
    }

    public static void commit() {
        
        try {
            local.get().commit();
        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_COMMIT);
        }
    }
    
    public static void rollback() {
        
        try {
            local.get().rollback();
        } catch (Exception e1) {
            throw new AppException(ErrorCode.FAILED_TO_ROLLBACK);
        }
    }
    
    public ConnectionService() {
        super(Constants.MAX_THREAD, Constants.MAX_THREAD, 0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
    }

    @Override
    protected void beforeExecute(Thread t, Runnable r) {
        super.beforeExecute(t, r);
        init();
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        super.afterExecute(r, t);
        release();
    }
}