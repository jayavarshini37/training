/*
Requirement:
    To create a connection between the database and java application.
    
Entity:
    JdbcConnection
    
Function declaration:
    public void createConnection() 
    public void closeConnection() 
    
Jobs to be done:
    
    1. Declare Connection and PreparedStatement as connection and statement.
    2. Declare createConnection method.
    	2.1 Load db.properties file to a properties object.
        2.2 Establish the connection using sql driver and store it as connection.
    3. Declare closeConnecton method.
        3.1 Close the Connection of the server.
        3.2 Close the PreparedStatement of the query.

Pseudo code:
class JdbcConnection {
    
    Connection connection;
	PreparedStatement statement;
	
	public void createConnection() {

		Properties properties = new Properties();
		try {
		    properties = new Properties();
		    InputStream resourceAsStream =  JdbcConnection.class.getClassLoader().getResourceAsStream("db.properties");
		    if (resourceAsStream != null) {
		        properties.load(resourceAsStream);
		    }

		} catch (IOException exception) {
		    throw new AppException(ErrorCode.IO_EXCEPTION);
		}

		try {
			connection = DriverManager.getConnection(properties.getProperty("URL"), properties.getProperty("User Name"), properties.getProperty("Password"));


		} catch (SQLException exception) {
			throw new AppException(ErrorCode.SQLException);
		}

	}

	public void closeConnection() {
		try {
			connection.close();
			statement.close();
		} catch (SQLException exception) {
			throw new AppException(ErrorCode.SQLException);
		}
	}
}
*/

package com.kpr.training.jdbc.connections;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;

public class Connections {

	public Connection connection;
	public PreparedStatement statement;

	public void createConnection() {

		Properties properties = new Properties();
		try {
			properties = new Properties();
			InputStream resourceAsStream =
					Connections.class.getClassLoader().getResourceAsStream("db.properties");
			if (resourceAsStream != null) {
				properties.load(resourceAsStream);
			}

		} catch (IOException exception) {
			throw new AppException(ErrorCode.IO_EXCEPTION);
		}

		try {
		    connection = DriverManager.getConnection(properties.getProperty("URL"),
					properties.getProperty("User Name"), properties.getProperty("Password"));


		} catch (SQLException exception) {
			throw new AppException(ErrorCode.SQLException);
		}

	}

	public void closeConnection() {
		try {
		    connection.close();
		    statement.close();
		} catch (SQLException exception) {
			throw new AppException(ErrorCode.SQLException);
		}
	}
}