package com.kpr.training.jdbc.services;

/*
Requirement:
    To perform the CRUD operation of the Person.
 
Entity:
    1.Person
    2.PersonService
    3.AppException
    4.ErrorCode
 
Function declaration:
    public long create(Person person, Address address)
    public Person read(long id, boolean addressFlag)
    public ArrayList<Person> readAll()
    public void update(long id, Person person, Address address)
    public void delete(long id)

Jobs To Be Done:
    1. Create a Person.
    2. Read a record in the Person.
    3. Read all the record in the Person.
    4. Update a Person.
    5. Delete a Person.
*/

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import com.kpr.training.jdbc.constants.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;

public class PersonService {

    @SuppressWarnings("static-access")
    public long create(Person person) {

        long personID = 0;
        long addressId;
        int numberOfRowsAffected;
        AddressService addressService = new AddressService();

        if (isNameUnique(0, person.getFirstName(), person.getLastName(), ThreadPool.get()) == true) {

            if (checkUniqueEmail(0, person.getEmail(), ThreadPool.get()) == true) {

                if (person.getAddress() != null) {

                    if (addressService.getAddressId(person.getAddress(),
                            ThreadPool.get()) > 0) {
                        addressId = addressService.getAddressId(person.getAddress(),
                                ThreadPool.get());
                    } else {
                        addressId = addressService.create(person.getAddress());
                    }
                } else {
                    addressId = 0;
                }

                try {
                    PreparedStatement ps = null;
                    ps = ThreadPool.get().prepareStatement(
                            QueryStatement.CREATE_PERSON_QUERY, ps.RETURN_GENERATED_KEYS);
                    ps.setString(1, person.getFirstName());
                    ps.setString(2, person.getLastName());
                    ps.setString(3, person.getEmail());
                    ps.setDate(4, new java.sql.Date(person.getBirthDate().getTime()));
                    ps.setLong(5, addressId);
                    numberOfRowsAffected = ps.executeUpdate();
                    ResultSet personId = ps.getGeneratedKeys();

                    if (personId.next()) {
                        personID = personId.getLong("GENERATED_KEY");
                    }
                    ps.close();

                    if (numberOfRowsAffected == 0 || personID == 0) {
                        throw new AppException(ErrorCode.PERSON_CREATION_FAILS);
                    }

                } catch (Exception e) {
                    throw new AppException(ErrorCode.PERSON_CREATION_FAILS, e);
                }
            } else {
                throw new AppException(ErrorCode.EMAIL_NOT_UNIQUE);
            }
        } else {
            throw new AppException(ErrorCode.FIRST_NAME_AND_LAST_NAME_DUPLICATE);
        }

        return personID;
    }

    public Person read(long id, boolean addressFlag) {

        Person person = null;
        AddressService addressService = new AddressService();

        try (PreparedStatement ps =
                ThreadPool.get().prepareStatement(QueryStatement.READ_PERSON_QUERY)) {

            ps.setLong(1, id);
            ResultSet result = ps.executeQuery();

            while (result.next()) {
                person = readPerson(result);
                if (addressFlag) {
                    person.setAddress(addressService.read(result.getLong("address_id")));
                }
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_PERSON_FAILS, e);
        }

        return person;
    }

    public ArrayList<Person> readAll() {

        ArrayList<Person> persons = new ArrayList<>();
        AddressService addressService = new AddressService();
        Person person;

        try (PreparedStatement ps =
                ThreadPool.get().prepareStatement(QueryStatement.READALL_PERSON_QUERY)) {

            ResultSet result = ps.executeQuery();

            while (result.next()) {
                person = readPerson(result);
                Address address = addressService.read(result.getLong("address_id"));
                person.setAddress(address);
                persons.add(person);
            }

        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_PERSON_FAILS, e);
        }
        return persons;
    }

    public void update(Person person) {

        int numberOfRowsAffected = 0;
        AddressService addressService = new AddressService();
        long addressId = 0;

        if (isNameUnique(person.getId(), person.getFirstName(), person.getLastName(), ThreadPool.get()) == true) {

            if (checkUniqueEmail(person.getId(), person.getEmail(),
                    ThreadPool.get()) == true) {

                if (person.getAddress() == null && addressId(person.getId(), ThreadPool.get()) == 0) {
                    addressId = 0;
                } else if (person.getAddress() != null && addressId(person.getId(), ThreadPool.get()) == 0) {

                    if (addressService.getAddressId(person.getAddress(),
                            ThreadPool.get()) > 0) {
                        addressId = addressService.getAddressId(person.getAddress(),
                                ThreadPool.get());
                    } else {
                        addressId = addressService.create(person.getAddress());
                    }
                } else if ((person.getAddress() != null && addressId(person.getId(), ThreadPool.get()) != 0)) {

                    if (addressService.getAddressId(person.getAddress(), ThreadPool.get()) > 0) {
                        addressId = addressService.getAddressId(person.getAddress(),
                                ThreadPool.get());
                    } else {
                        addressId = addressService.create(person.getAddress());
                    }

                    if (addressUsage(addressId(person.getId(), ThreadPool.get()), ThreadPool.get()) != true) {
                        addressService.delete(addressId(person.getId(), ThreadPool.get()));
                    }
                } else if ((person.getAddress() == null && addressId(person.getId(), ThreadPool.get()) != 0)) {
                    addressId = addressId(person.getId(), ThreadPool.get());
                }

                try (PreparedStatement ps = ThreadPool.get()
                        .prepareStatement(QueryStatement.UPDATE_PERSON_QUERY.toString())) {

                    ps.setString(1, person.getFirstName());
                    ps.setString(2, person.getLastName());
                    ps.setString(3, person.getEmail());
                    ps.setLong(4, addressId);
                    ps.setDate(5, new java.sql.Date(person.getBirthDate().getTime()));
                    ps.setLong(6, person.getId());
                    numberOfRowsAffected = ps.executeUpdate();

                    if (numberOfRowsAffected == 0) {
                        throw new AppException(ErrorCode.PERSON_UPDATION_FAILS);
                    }
                } catch (Exception e) {
                    throw new AppException(ErrorCode.PERSON_UPDATION_FAILS, e);
                }
            } else {
                throw new AppException(ErrorCode.EMAIL_NOT_UNIQUE);
            }
        } else {
            throw new AppException(ErrorCode.FIRST_NAME_AND_LAST_NAME_DUPLICATE);
        }
    }

    public void delete(long id) {

        int numberOfRowsAffected = 0;

        if (addressId(id, ThreadPool.get()) != 0) {
            
            if (addressUsage(addressId(id, ThreadPool.get()), ThreadPool.get()) != true) {
                AddressService addressService = new AddressService();
                addressService.delete(addressId(id, ThreadPool.get()));
            }
        }

        try (PreparedStatement ps =
                ThreadPool.get().prepareStatement(QueryStatement.DELETE_PERSON_QUERY)) {

            ps.setLong(1, id);
            numberOfRowsAffected = ps.executeUpdate();
            ps.close();

            if (numberOfRowsAffected == 0) {
                throw new AppException(ErrorCode.PERSON_DELETION_FAILS);
            }

        } catch (Exception e) {
            throw new AppException(ErrorCode.PERSON_DELETION_FAILS, e);
        }
    }


    public boolean checkUniqueEmail(long id, String email, Connection con) {

        ResultSet result;
        boolean unique = true;
        long personId = 0;

        try (PreparedStatement ps = con.prepareStatement(QueryStatement.EMAIL_UNIQUE)) {

            ps.setString(1, email);
            result = ps.executeQuery();

            if (result.next()) {
                personId = result.getLong("id");
            }

            if (id == 0) {
                unique = personId > 0 ? false : true;
            } else {
                unique = (personId > 0 && personId != id) ? false : true;
            }

        } catch (Exception e) {
            throw new AppException(ErrorCode.FAILED_TO_CHECK_EMAIL, e);
        }
        return unique;
    }

    public long addressId(long id, Connection con) {

        long addressId = 0;
        try (PreparedStatement ps =
                con.prepareStatement(QueryStatement.GET_ADDRESS_ID)) {

            ps.setLong(1, id);
            ResultSet result = ps.executeQuery();

            if (result.next()) {
                addressId = result.getLong("address_id");
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_ADDRESSID_FAILS, e);
        }
        return addressId;
    }

    public Person readPerson(ResultSet result) {

        Person person = null;

        try {
            person = new Person(result.getString("first_name"), result.getString("last_name"),
                    result.getString("email"), new java.util.Date(result.getDate("birth_date").getTime()));
            person.setCreatedDate(result.getTimestamp("created_date"));
            person.setId(result.getLong("id"));
        } catch (Exception e) {
            throw new AppException(ErrorCode.READING_PERSON_FAILS, e);
        }

        return person;
    }

    public boolean addressUsage(long addressId, Connection con) {

        boolean usage = true;

        try (PreparedStatement ps =
                con.prepareStatement(QueryStatement.ADDRESS_USAGE)) {
            ps.setLong(1, addressId);
            ResultSet result = ps.executeQuery();

            if (result.next()) {
                int count = result.getInt("COUNT(person.id)");

                if (count > 1) {
                    usage = true;
                } else if (count == 1) {
                    usage = false;
                }
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.CHECKING_ADDRESS_USAGE_FAILS, e);
        }

        return usage;
    }
    
    public java.util.Date dateValidator(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        java.util.Date utilDate = null;
        boolean valid = false;
        
        try {
            LocalDate.parse(date,
                    DateTimeFormatter.ofPattern("dd-MM-uuuu")
                            .withResolverStyle(ResolverStyle.STRICT));
            
            valid = true;
        } catch (Exception e) {
            valid = false;
            throw new AppException(ErrorCode.WRONG_DATE_FORMAT, e);
        }
        
        if (valid == true) {
            try {
                utilDate = formatter.parse(date);
            } catch (Exception e) {
                throw new AppException(ErrorCode.WRONG_DATE_FORMAT, e);
            }
        }
        return utilDate;
    }
    
    public boolean isNameUnique(long id, String firstName, String lastName, Connection con) {

        boolean unique = true;
        long personId = 0;

        try (PreparedStatement ps =
                con.prepareStatement(QueryStatement.NAME_UNIQUE)) {
            ps.setString(1, firstName);
            ps.setString(2, lastName);
            ResultSet result = ps.executeQuery();

            if (result.next()) {
                personId = result.getLong("id");
            }
            
            if (id == 0) {
                unique = personId > 0 ? false : true;
            } else {
                unique = (personId > 0 && personId != id) ? false : true;
            }
        } catch (Exception e) {
            throw new AppException(ErrorCode.CHECKING_UNIQUE_NAME_FAILS, e);
        }

        return unique;
    }
}
