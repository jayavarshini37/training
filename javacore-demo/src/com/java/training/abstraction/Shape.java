/*Requirement:
 To Demonstrate abstract classes using shape class.
 
Entities:
  shape 
  Rectangle,
  Circle,
  TestAbstraction1 
  
Function Declaration:
  abstract void draw()
  void draw()  
  public static void main(String[] args)

Jobs to be Done:
 1.Create an abstract class shape and declare a function named draw.
 2.Create a class Rectangle and inherit shape.
 3.Create a class Circle1 and inherit shape.
 4.Create a main class TestAbstraction1 and create an object for shape.
*/
package com.java.training.abstraction;
abstract class Task {  

    abstract void draw();  
}  
class Rectangle1 extends Task {  
    
	void draw() {
        System.out.println("drawing rectangle");
  }  
}  
class Circle1 extends Task {  
    
	void draw() {
        System.out.println("drawing circle");
  }  
}  
class TestAbstraction1 {  
    
	public static void main(String[] args) {  
        Task s = new Circle1();
        s.draw();  
  }  
}