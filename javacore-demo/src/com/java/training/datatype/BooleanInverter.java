/*Requirement:
   Invert the value of a boolean and which operator would you use?
    
Entity:
    BooleanInverter
	
Function declaration:
    public static void main(String[] args)
Jobs TO be Done

    1.Inverting the Boolean using the ! Symbol.
    2.Converting the true value of the Boolean to false using the ! Symbol..
*/
package com.java.training.datatype;    

public class BooleanInverter {
	
    public static void main(String[] args) {
        boolean flag = false;
        String res = "Fail";
        if(res == "Pass") {
            System.out.println(flag);
        } else {
            flag = !flag;
            System.out.println(flag);
        }
    }
}