/*Requirements

    Create a program that reads an unspecified number of integer arguments from the command line and adds them together.
    For example, suppose that you enter the following: java Adder 1 3 2 10
    The program should display 16 and then exit. The program should display an error message if the user enters only one argument.
	
Entities

     Adder
	 
Function Declaration

    public static void main(String[] args)

Jobs to be done

    1. Create a class Adder
	2. If the value is less than two,it prints the given data 
	3. It should throw error when program contains one argument
	4. Using the for loop, convert the String arguments to integer using parseInt of Integer Class.
    5. Then add the argument values into sum and print the 
	6. exit the program
*/
package com.java.training.datatype;
 
public class Adder {
	 
    public static void main(String[] args) {
	    int numArgs = args.length;

	//this program requires at least two arguments on the command line
        if (numArgs < 2) {
            System.out.println("This program requires two command-line arguments.");
        } else {
	    int sum = 0;
		for (int i = 0; i < numArgs; i++) {
            sum += Integer.valueOf(args[i]).intValue();
	    }

	    //print the sum
            System.out.println(sum);
        }
    }
}