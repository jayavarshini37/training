/*Requirements:

   Change MaxVariablesDemo to show minimum values instead of maximum values. You can delete all code related to the variables aChar and aBoolean.
  What is the output?
  
entities:

    MinVariablesDemo 
	
Function Declaration

     public static void main(String args[])
	
Jobs to be done

    1. create class called MinVariableDemo and declare the main methood
	2. Use constant "MIN_VALUE" for finding the minimum possible values
	3. Declare the variable smallestByte is of type byte and find the minimum value of byte using constant "MIN_VALUE" and store in smallestByte
    4. Declare the variable smallestShort is of type short and find the minimum value of short using constant "MIN_VALUE" and store in smallestShort
    5. Declare the variable smallestInteger is of type int and find the minimum value of int using constant "MIN_VALUE" and store in smallestInteger
    6. Declare the variable smallestLong is of  type long  and find the minimum value of long using constant "MIN_VALUE" and store in smallestLong
    7. Declare the variable smallestFloat is of  type float short and find the minimum value of float using constant "MIN_VALUE" and store in smallestFloat
    8. Declare the variable smallestDouble is of type Double and find the minimum value of double using constant "MIN_VALUE" and store in smallestDouble
	9. display all the output using "System.out.println".
*/
package com.java.training.datatype;

public class MinVariablesDemo {
    public static void main(String args[]) {

        // integers
        byte smallestByte = Byte.MIN_VALUE;
        short smallestShort = Short.MIN_VALUE;
        int smallestInteger = Integer.MIN_VALUE;
        long smallestLong = Long.MIN_VALUE;

        // real numbers
        float smallestFloat = Float.MIN_VALUE;
        double smallestDouble = Double.MIN_VALUE;

        // display them all
        System.out.println("The smallest byte value is " + smallestByte);
        System.out.println("The smallest short value is " + smallestShort);
        System.out.println("The smallest integer value is " + smallestInteger);
        System.out.println("The smallest long value is " + smallestLong);
        System.out.println("The smallest float value is " + smallestFloat);
        System.out.println("The smallest double value is " + smallestDouble);
    }
}

/*output

The smallest byte value is -128
The smallest short value is -32768
The smallest integer value is -2147483648
The smallest long value is -9223372036854775808
The smallest float value is 1.4E-45
The smallest double value is 4.9E-324
*/