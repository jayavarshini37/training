
/*
Requirement:
    To demonstrate Overloading with Wrapper types
    
Entity:
    WrapperDemo 
    WrapperOverLoad 
    
Function declaration:
    void wrapIntSetter(int a)
	void wrapcharSetter(char b)
	void wrapDoubleSetter(double c)
	public static void main(String[] args)

Jobs to be  Done
    1.Create a class WrapperDemo
    2. Create a method wrapIntSetter which gets the Primitive type int as the parameter and prints it.
    3.Create a method wrapCharSetter which gets the Primitive type char as the parameter and prints it.
    4.Create a method wrapDoubleSetter which gets the Primitiev type double as the parameter and prints it.
    5.Create another class WrapperOverLoad for overloading
	6.Assume the values for char,int and double
    7.Call the wrapCharSetter method and pass the Wrapper type Character as the argument.
    8.Call the warpDoubleSetter method and pass the Wrapper type Double as the argument.
*/
package com.java.training.datatype;

class WrapperDemo {
    
	void wrapIntSetter(int a) {
        System.out.println("int Value changed to Integer "+a);
    }
    void wrapcharSetter(char b) {
        System.out.println("char Value changed to Character "+b);
    }
    void wrapDoubleSetter(double c) {
        System.out.println("double Value changed to Double "+c);
    }
}

public class WrapperOverload {
   
   public static void main(String[] args) {
        //Overloading Wrapper types//
        
        Integer sd = 65;
        Character chara = 'S';
        Double fin = 3.14;
        WrapperDemo wrap = new WrapperDemo();
        wrap.wrapIntSetter(sd);
        wrap.wrapcharSetter(chara);
        wrap.wrapDoubleSetter(fin);
    }
}