/*Requirement:
  To print the type of the result value of following expressions
  
Entity:
   Main
   
Function declaration:
   public static void main(String[] args)

*/
package com.java.training.datatype;

class Main {
	
	public static void main(String[] args) {
		
        //System.out.println(x/c);
        Object o = 100 / 24;
	    System.out.println(o.getClass().getName());
	    Object a = 100.10 / 10;
	    System.out.println(a.getClass().getName());
	    Object b = 'Z' /2;
	    System.out.println(b.getClass().getName());
	    Object c =10.5 /0.5 ;
	    System.out.println(c.getClass().getName());
	    Object d = 12.4 % 5.5;
	    System.out.println(d.getClass().getName());
	    Object e = 100/56;
	    System.out.println(e.getClass().getName());
	}
}