/*
Requirement:
    To find fibonacci series using Recursion.

Entity:
   FibonacciUsingRecursion 

Function Declaration:
   public static void printFibonacci(int i)
   public static void main(String[] args)
   printFibonacci
	
Jobs to be Done:
    1. Declare the class FibonacciUsingRecursion.
    2. Declare and assign the first two variable as one and zero and declare the third variable sum.
    3. Declare the function as public static void printFibonacci(int i) and give the body of
       the function.
    4. Call the function printFibonacci(int i) by declaring the variable n.
    5. Print the series using print statement.
*/
package com.java.training.controlflow;

import java.util.Scanner;

public class FibonacciUsingRecursion {
        
    public static int x = 0;
    public static int y = 1;
    public static int sum;
    public static void printFibonacci(int i) {
            
	    if(i > 0) {
            sum = x + y;
            x = y;
            y = sum;
            System.out.print(x + " ");
            printFibonacci(i - 1);
            }
        }
    public static void main(String[] args) {
        
		int n;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        printFibonacci(n);
    }
}