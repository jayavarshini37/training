/*
Requirement:
    To find fibonacci series using while loop.

Entity:
    FibonacciUsingWhile 

Function Declaration:
    public static void main(String[] args)
	
Jobs to be Done:
    1. Declare the class FibonacciUsingWhile.
    2. First declare the range to which the Series to be printed and assign the first two numbers of
       the series under two different variables as x and y.
    3. Use while loop. Assign the value to the new variable i. Continue the process till the 
       condition meets i less than or equal to n.
    4. Add the values of x and y and store it in new variable sum.
    5. Assign the value of y to x and the value of y to sum and then increments the value of i.
    6. Print x.
*/
package com.java.training.controlflow;

import java.util.Scanner;

public class FibonacciUsingWhile {
    
	public static void main(String[] args) {
        
	   	int n;
		int x;
        int y;
	    int i;
        int sum;
        x = 0;
        y = 1;
        i = 1;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        while(i <= n) {
            sum = x + y;
            x = y;
            y = sum;
            i++;
            System.out.print(x + " ");
        }
    }
}