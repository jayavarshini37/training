/*
Requirement:
    To demonstrate object equality using Object.equals() vs ==,using String objects.

Entity:
    ObjectEquality

Function declaration:
    public static void main(String[] args)

Jobs to be done:
    1.Create a class ObjectEquality.
    2.Declare the objects word1 and word2 as value java.
    3.For ' == ' operator it returns false because it doesn't have same address.
    4.For Equals() function, it returns true as the values are same.
*/
package com.java.training.javalang;
public class ObjectEquality {
    public static void main(String[] args) {

        String word1 = new String("java");
        String word2 = new String("java");
        System.out.println(word1 == word2);
        System.out.println(word1.equals(word2));
    }
}