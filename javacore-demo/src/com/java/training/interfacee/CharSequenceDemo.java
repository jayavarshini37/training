/*
Requirement:
    To write a class that implements the CharSequence interface found in the java.lang package
    and the implementation should return the string backwards.

Entity:
    CharSequenceDemo,CharSequence

Function declaration:
    toCharArray()
    clone()
    length()
    subSequence()
    toString()
    charAt()
    public static void main(String[] args)

Jobs to be done:
    1.Import CharSequence from java.lang.package.
    2.Declare a class CharSequenceDemo that implements CharSequence.
    3.Declare a varaiable charSequence of type String.
    4.Give a constructor.
    5.Get the string and store it in strArray of char[] type.
    6.Take a clone of the strArray and upto a length of the String reverse the string and print it.
    7.Code for the given question using length(),charAt(),subSequence() and toString().
    8.Print the results.
*/

package com.java.training.interfacee;

import java.lang.CharSequence;

public class CharSequenceDemo implements CharSequence {

    public String sentence;
    public CharSequenceDemo(String sentence) {
    char[] strArray = sentence.toCharArray();
    char[] reversedArray = strArray.clone();
    int j = strArray.length - 1;
    for (int i = 0; i < strArray.length; i++) {
       reversedArray[j] = strArray[i];
        j--;
    }
    this.sentence = new String(reversedArray);
    System.out.println("Reverse of the String is" + " " + this.sentence);
    }

    public int length() {
        return sentence.length();
    }

    public char charAt(int i) {
        return sentence.charAt(i);
    }

    public CharSequence subSequence(int i, int i1) {
        return sentence.subSequence(i, i1);
    }

    public String toString() {
        return sentence;
    }

    public static void main (String[] args) {
        
		String sentence = "Have a good days";
        CharSequenceDemo sentence1 = new CharSequenceDemo(sentence);
        System.out.println("length : " + " " + sentence1.length());
        System.out.println("charAt : " + " " + sentence1.charAt(2));
        System.out.println("subSequence : = " + " " + sentence1.subSequence(1,6));
        System.out.println("toString : " + " " + sentence1.toString());
    }
}