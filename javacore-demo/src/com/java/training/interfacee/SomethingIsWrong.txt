/*
Requirement:
What is wrong with the following interface?
public interface SomethingIsWrong {
    void aMethod(int aValue) {
        System.out.println("Hi Mom");
    }
}

Entity:
  SomethingIsWrong
  
Function declaration:
  void aMethod
*/
  
Answer:
  It has a method implementation in it. 
  Only default and static methods have implementations.
