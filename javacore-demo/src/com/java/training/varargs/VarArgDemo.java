/*
Requirement:
    To demonstrate the method overloading using varArgs.

Entity:
    VarArgDemo

Function declaration:
    public static void main(String[] args)
    public void check(int ... numbers)
    public void check(String word, int numbers)

Jobs to be done:
    1.Create the class VarArgDemo as public.
    2.Declare the function named check and number as a arguements.
    3.Print the number of integer values given and print that values given by using for each loop.
    4.Now declare the same method name as test now pass String word and group of integer values
      as a arguements.
    5.Now print the message given and the length of the integer values.
    6.Print the the given integer using for each loop.
*/
package com.java.training.varargs;

public class VarArgDemo {
   
    public void check(int ... numbers) {
       
	    System.out.println("Number of integer values: " + " " + numbers.length );
            for (int values : numbers) {
                System.out.println("The numbers are: " + " " + values);
        }
    }
    
    public void check(String word, int ... numbers) {
        
		System.out.println(word + " the number of values" + " " + numbers.length);
        for (int integer : numbers) {
            System.out.println(" the integer are " + " " + integer + " ");
        }
    }

    public static void main(String[] args) {
        
		VarArgDemo variable = new VarArgDemo();
        variable.check("count", 1,2,3,4,5);
    }
}