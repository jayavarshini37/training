/*
Requirement:
       To create a class named Dog and define a function named sound.

Entities:
       Dog

Function Declaration:
       void sound()
    
Jobs to be done:
    1. Declare the class Dog.
    2. call the method sound().
    3. Print the the statement as given.
*/
package com.java.training.inheritance;

public class Dog extends Animal {
    public void sound() {
        System.out.println("Barks");
    }
}