/*
Requirement:
    To create a class named Cat and define a function named sound.
	
Entity:
    Cat 

Function Declaration:
    void sound()

Jobs to be done:
    1. Declare the class Cat.
    2. call the method sound().
    3. Print the the statement as given.
*/


package com.java.training.inheritance;

public class Cat extends Animal {
    public void sound() {
        System.out.println("Meows");
    }
}