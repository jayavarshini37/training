/*
Requirement:
   To create a class named Snake and define a function named sound.
   
Entities:
    Snake.

Function Declaration:
    public void sound().

Jobs to be done:
    1. Declare the class Snake.
    2. call the method sound().
    3. Print the statement as given.
*/



package com.java.training.inheritance;

public class Snake extends Animal {
    public void sound() {
        System.out.println("Hissing");
    }
}