/*
Requirement:
 To find whether the following interface is valid or not
 public interface Marker {
}
 
Entity:
 Marker
 
Function declaration:
 No function is declared here
*/
 
Answer:
 Yes. Methods are not required.
 Empty interfaces can be used as types and to mark classes without requiring any particular method implementations.
 For an example of a useful empty interface, see java.io.Serializable.

