*Requirement:
    To find what is the initial capacity of the following string builder?
    StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.");

Entity:
     No entity is defined here 
    
Function declaration:
    No function is declared here

*/
SOLUTION:
    Length of the given String = 26
    Initial capacity of the string = 26 + 16 = 42.