/*
Requirement:
    To write a program that computes my initials of my full name and displays them.

Entity:
    MyInitials.

Function declaration:
    public static void main(String[] args) 

Jobs to be done:
    1.Create a class MyInitial.
    2.Give your name as the input and add a space before your name.
    3.Traverse our input by using for loop.
    4.For initials, the character next to the space must be upper case.
    5.print the character separated by space.
*/

//Program:
import java.util.Scanner;

public class MyInitial {
    
	public static void main(String[] args) {
        
		String name;
        Scanner ip = new Scanner(System.in);
        name = ip.nextLine();
        name = " " + name;
        for(int i = 0;i < name.length();i++) {
            if(name.charAt(i) == ' ' && Character.isUpperCase(name.charAt(i + 1))) {
                System.out.print(name.charAt(i + 1) + " ");
            }
        }
    }
}