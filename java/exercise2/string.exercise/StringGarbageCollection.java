/*
Requirement:
   The following code creates one array and one string object.To check how many references to those objects 
exist after the code executes and the object eligible for garbage collection or not.

String[] students = new String[10];
String studentName = "Peter Parker";
students[0] = studentName;
studentName = null;


Entity:
   No entity is declared here

Function declaration:
   No function is declared here
*/


Answer: There is one reference to the students array and that array has one reference to the string 
Peter Parker. 
        No object is eligible for garbage collection. 
		The array students is not eligible for garbage collection because it has one reference to the object studentName even though that object 
has been assigned the value null.
        The object studentName is not eligible either because students[0] 
still refers to it.