 /*
 Requirements:
    sort and print following String[] alphabetically ignoring case. Also convert and print even indexed Strings into uppercase
 { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }
Entites:
    PlaceSort
Function Declaration:
     public static void main(String[] args) 
Jobs to be done:
              1.Create a public class PlaceSort.
              2.Inside main function,give vales to string.
			  3.sort the given array.
			  4.print the sorted array.
			  5.using for loop,and if condition,change the string values to uppercase,
 
 */
import java.util.Arrays;

public class PlaceSort {
    
	public static void main(String[] args) {
        
		String [] place = { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        System.out.println("Given Array :" + Arrays.toString(place));
        Arrays.sort(place, String.CASE_INSENSITIVE_ORDER);
        for(int i = 0;i < place.length;i++){
            if(i%2 == 0) {
                place[i] = place[i].toUpperCase();
            }
        }
        System.out.println("Sorted Array :" + Arrays.toString(place));
    }
}
