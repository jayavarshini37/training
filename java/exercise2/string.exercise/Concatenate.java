/*Requirement:
    To show two ways to concatenate the following two strings together to get the string "Hi, mom ":
    String hi = "Hi, ";
    String mom = "mom.";
    
Entities:
    There is no entity given in this program.

Function:
    There is no function declared in this program.
	
Jobs to be done:
    1) By using the addition operator,we can concatenate.
    2) By using concat() function,we can concatenate.
*/
SOLUTION:
    1) hi + mom
    2) hi.concat(mom)