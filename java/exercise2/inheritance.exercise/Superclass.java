/*
Requirement:
    To consider the following classes and answer the following questions.
     public class ClassA {
        public void methodOne(int i) {
        }
        public void methodTwo(int i) {
        }
        public static void methodThree(int i) {
        }
        public static void methodFour(int i) {
        }
    }

    public class ClassB extends ClassA {
        public static void methodOne(int i) {
        }
        public void methodTwo(int i) {
        }
        public void methodThree(int i) {
        }
        public static void methodFour(int i) {
        }
    }
    a. Which method overrides a method in the superclass?
    b. Which method hides a method in the superclass?
    c. What do the other methods do?

Entity:
    A and B 

Function Declaation:
    public void methodOne(int i)
    public void methodTwo(int i)
    public static void methodThree(int i)
    public static void methodFour(int i)
    public static void methodOne(int i)
    public void methodTwo(int i)
    public void methodThree(int i)
    public static void methodFour(int i)
*/


Question 1a: Which method overrides a method in the superclass?
Answer 1a: methodTwo

Question 1b: Which method hides a method in the superclass?
Answer 1b: methodFour

Question 1c: What do the other methods do?
Answer 1c: They cause compile-time errors.