/*
Requirement:
    To create a class named Cat and define a function named sound.
	
Entity:
    Cat 

Function Declaration:
    void sound()

Jobs to be done:
    1. Declare the class Cat.
    2. call the method sound().
    3. Print the the statement as given.
*/



public class Cat extends Animal1 {
    public void sound() {
        System.out.println("Meows");
    }
}