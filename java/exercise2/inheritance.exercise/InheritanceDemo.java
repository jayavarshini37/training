/*
Requirement:
   To demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects

Entity:
   InheritanceDemo 

Function Declaration:
   public static void main(String[] args)

Job to be done:
   1. create a class named InheritanceDemo
   2. create object for classes Animal1,Cat,Snake,Dog.
   3. using the objects call the functions. 
*/


public class InheritanceDemo{
    public static void main(String[] args){
        Animal1 animal = new Animal1();
        Dog dog = new Dog();
        Cat cat = new Cat();
        Snake snake = new Snake();
        animal.sound();
        animal.run();
        animal.run(7);
        cat.sound();
        cat.run();
        cat.run(6);
        dog.sound();
        dog.run();
        dog.run(8);
        snake.sound();
        snake.run();
        snake.run(9);
    }
}