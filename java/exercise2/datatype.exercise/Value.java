/*Requirement

What is the value of the following expression, and why?
Integer.valueOf(1).equals(Long.valueOf(1))
	 
Entities

     No entity
	 
Function Declaration

     No function  is declared here
*/
	 
Answer:	 
Integer.valueOf(1).equals(Long.valueOf(1))
The two objects (the Integer and the Long) have different types.