/*
Requirement:
    To create a program that is similar to the previous one but instead of reading integer
    arguments,it reads floating-point arguments.It displays the sum of the arguments, using
    exactly two digits to the right of the decimal point.

Entity:
    FloatAdderDemo

Function declaration:
    public static void main(String[] args)
    format()

Jobs to be done:
    1.Import the package java.text.DecimalFormat.
    2.Create a class FloatAdderDemo.
    3.Change string type arguements into float.
    4.If the length of the arguements is more than two then print the sum of all the arguements.
    5.If the length of the arguements is less than two print the if statement.
    6.Format the total.
    7.Give the arguments in run command.
*/

//Program:
import java.text.DecimalFormat;

public class FloatAdderDemo {
    
	public static void main(String[] args) {
        float number1 = Float.parseFloat(args[0]);
        if (args.length < 2) {
            System.out.println("Enter 2 or more value");
        } else {
        double sum = 0.0;
        for (int i = 0; i < args.length; i++) {
                sum += Double.valueOf(args[i]).doubleValue();
        }

        DecimalFormat myFormatter = new DecimalFormat("###,###.##");
        String total = myFormatter.format(sum);
            System.out.println(total);
        }
    }
}