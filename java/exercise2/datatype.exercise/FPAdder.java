/*Requirements

    Create a program that is similar to the previous one but instead of reading integer arguments, it reads floating-point arguments.
    It displays the sum of the arguments, using exactly two digits to the right of the decimal point.
    For example, suppose that you enter the following: java FPAdder 1 1e2 3.0 4.754
    The program would display 108.75. Depending on your locale, the decimal point might be a comma (,) instead of a period (.).
	
Entities

    FPAdder 
	
Function Declaration

     public static void main(String[] args)
	
jobs to be done

    1. create the class FPAddder and declare the main method
	2. store the length of the args in numArgs
	3. check the numArgs is less than 2, if it is less than two print the statement "This program requires two command-line arguments"
	4. Else  declare variable sum is of type double and assign the value as 0.0
	5. Using "for loop" get the values 
	6. Format the  sum by creating object
	7. Declare the variable as output is of type String 
	8. And format the sum 
	9. print the output

Answer:*/
  

import java.text.DecimalFormat;

public class FPAdder {
    public static void main(String[] args) {

	int numArgs = args.length;

	//this program requires at least two arguments on the command line
        if (numArgs < 2) {
            System.out.println("This program requires two command-line arguments.");
        } else {
	    double sum = 0.0;

	    for (int i = 0; i < numArgs; i++) {
                sum += Double.valueOf(args[i]).doubleValue();
	    }

	    //format the sum
	    DecimalFormat myFormatter = new DecimalFormat("###,###.##");
	    String output = myFormatter.format(sum);

	    //print the sum
            System.out.println(output);
        }
    }
}