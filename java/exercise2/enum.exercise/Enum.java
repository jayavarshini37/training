/*Requirement:
  To compare the enum values using equal method and == operator

Entities:
  No entities used for this question.
 
Function Declaration:
  No function is declared.
  
Job to be done:
  1.Comparison using equal method
  2.Comparison using == operator*/
 
Solution:
   enum: An enum is a special "class" that represents a group of constants (unchangeable variables, like final variables).
      
	  
	  enum Level 
	  { 
	   HIGH, 
       MEDIUM
	  };

      Level var = Level.HIGH;
      if (var == Level.MEDIUM); 
      if (var.equals(Level.MEDIUM)); 