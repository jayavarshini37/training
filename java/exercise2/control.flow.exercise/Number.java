/*Requirement:
    To find the output of the given program if the value of aNumber is 3.
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

Entities:
    It doesn't have any class name

Function Declaration:
    There is no function is declared in this program.

Jobs To Be Done:
    1)Considering the given program
    2)Checking the aNumber value greater than or equal to zero.
    3)It is true, So it moves to the next statement.
    4)Checking the aNumber value equal to zero 
    5)It is false, So it skips the step and move to the else part.
    6)There is no else part in these if type. So it moves to the else of previous if part.
    7)It prints the second string and followed by third string.
*/
OUTPUT:
second string
third string