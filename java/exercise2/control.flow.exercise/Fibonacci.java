/*
Requirement:
    To find fibonacci series using for loop.

Entity:
    Fibonacci 

Function Declaration:
    public static void main(String[] args)

Jobs to be Done:
    1. Declare the class Fibonacci.
    2. First declare the range to which the Series to be printed and assign the first two numbers of
       the series under two different variables as a and b.
    3. Use for loop.Continue the process till the condition meets i less than or equal to n.
    4. Add the values of a and b and store it in new variable sum.
    5. Assign the value of b to a and the value of b to sum.
    
*/

import java.util.Scanner;

public class Fibonacci {

public static void main(String[] args) {
    
	Scanner sc = new Scanner(System.in);
    int n;
	int i;
	int sum;
	int a = 0;
	int b = 1;
    System.out.println("Enter the maximum number");
    n = sc.nextInt();
    for(i = 1;i <= n;i++) {
        System.out.println(a + " ");
        sum = a + b;
        a = b;
        b = sum;
  }
 }
}