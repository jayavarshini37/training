/*
Requirement:

What Integer method can you use to convert an int into a string that expresses the number in hexadecimal?
For example, what method converts the integer 65 into the string "41"?

Entity:
  No entity
  
Function declaration:
  No function is declared here
*/

Answer: toHexString