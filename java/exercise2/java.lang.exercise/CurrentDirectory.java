/*
/*
Requirement:
    To print the absolute path of the .class file of the current class.

Entity:
    CurrentDirectory

Function declaration:
    public static void main(String[] args)

Jobs to be done:
    1.Declare the class CurrentDirectory.
    2.Under the main method declare a variable called path of type String, this would get a current
      user directory of a java class file.
    3.Print that directory.
*/

//Program:
public class CurrentDirectory {
    public static void main(String[] args) {

        String path = System.getProperty("user.dir");
        System.out.println("File Directory = " + path);

    }
}