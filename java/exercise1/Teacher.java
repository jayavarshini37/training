public class Teacher{ 
  //Access Specifiers 
    public String teacherId;  
    private String teacherName;
    protected String dateOfJoining;
    public Teacher(String teacId,String teacName,String teacdoj) { //Parameterized Constructor 
        teacherId = teacId;
        teacherName = teacName;
        dateOfJoining = teacdoj;
    }
    public void printInfo() {  // Method Declaration
        System.out.println("TeacherName: " + teacherName);
        TeacherDept teacdept = new TeacherDept("Biology");
        teacdept.display(); 
    }
    class TeacherDept{  
        private String teacherDept;
        
        public TeacherDept(String teacDept) {
            teacherDept = teacDept;
        }
        public void display() {
            System.out.println("TeacDept: " + teacherDept);
        }
    }
}
class Main{
    public static void main(String args[]) {
        Teacher teac = new Teacher("1011","Jayavarshini","09-01-2000");
        teac.printInfo(); // To display Private Properties Using Methods
        System.out.println("TeacherId: " + teac.teacherId); //Prints EmpID
        System.out.println("Teacherdoj: " + teac.dateOfJoining);
    }
}