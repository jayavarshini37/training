abstract class AbstractDemo{
   public void myMethod(){
      System.out.println("Hello");
   }
   abstract public void anotherMethod();
}
public class Demo extends AbstractDemo{

   public void anotherMethod() { 
        System.out.print("Abstract method"); 
   }
   public static void main(String args[])
   { 
      //error: You can't create object of it
      AbstractDemo obj = new AbstractDemo();
      obj.anotherMethod();
   }
}

---------
An abstract class can not be instantiated, which means we are not allowed to create an object of it.
If we create an object of the abstract class and calls the method having no body(as the method is pure virtual) it will give an error. 
That is why we cant create object of abstract class.
When we compile the above code   AbstractDemo obj = new AbstractDemo();
 we will get the Error message like: 
   "Cannot create an instance of the abstract class or interface" 